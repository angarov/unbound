<?php
get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<div class="container">
				<header class="title">
				</header>
				
				<div class="partners events">
					
					<ul class="partners-list">
					<?php
						
						if( have_rows('news') ):

						    while ( have_rows('news') ) : the_row();
						    	//if(get_sub_field('upcoming')) {
/*
						    		$image = get_sub_field('image');
						    		var_dump($image);
						    		die();
						    		
				
*/
/*						    	$date = substr(get_sub_field('date'), 0, strpos(get_sub_field('date'), ",", strpos(get_sub_field('date'), ",") + 1));
						    	echo $date;

						    	$data = array (
								   'post_type' => 'news',
								   'post_title' => get_sub_field('title'),
								   'post_content' => get_sub_field('description'),
								   'post_status' => 'publish',
								   'post_date' => date('Y-m-d H:i:s', strtotime($date)),
								   'comment_status' => 'closed',   // if you prefer
								   'ping_status' => 'closed',      // if you prefer
								);
								print_r($data);
								//echo $post_id = wp_insert_post($data);
								if ($post_id) {
								   // insert post meta
								   add_post_meta($post_id, 'news_date', get_sub_field('date'));
								   add_post_meta($post_id, 'news_link', get_sub_field('link'));
								}
*/
						?>
						    <li>
						    	<div class="logo"><img src="<?php echo $image['url']; ?>"></div>
						    	<small><?php the_sub_field('location'); ?></small>
						    	<h2><?php the_sub_field('title'); ?></h2>
						    	<p><?php the_sub_field('description'); ?></p>
						    	<a href="<?php the_sub_field('link'); ?>" target="_blank" class="cta">( Schedule a meeting )</a>
						    </li>
						        
						<?php
								//}
						    endwhile;
						
						else :
						
						    // no rows found
						
						endif;
					?>
					</ul>
				</div>
			</div>
			
			<div class="past-events">
				<div class="container">
					<ul>
						<li><h2>Past Events</h2></li>
					<?php
						
						if( have_rows('events') ):

						    while ( have_rows('events') ) : the_row();
						    	if(!get_sub_field('upcoming')) {
						    		$image = get_sub_field('image');
						?>
						    <li>
						    	<img src="<?php echo $image['url']; ?>">
						    	<h3><?php the_sub_field('title'); ?></h3>
						    	<small><?php the_sub_field('location'); ?></small>
						    </li>
						        
						<?php
								}
						    endwhile;
						
						else :
						
						    // no rows found
						
						endif;
					?>
					</ul>
				</div>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();