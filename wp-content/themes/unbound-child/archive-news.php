<?php
/**
* Template Name: News
*
*/
get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<div class="container">
				<header class="title mid center" style="background-image: url(/wp-content/uploads/2017/12/image-1.jpg);">
		            <h1>In the News</h1>
				</header>
			</div>
			<div class="past-events">
				<div class="container">
					<?php
					if ( have_posts() ) : ?>
			
						<?php
						/* Start the Loop */
						while ( have_posts() ) : the_post();
			
							/*
							 * Include the Post-Format-specific template for the content.
							 * If you want to override this in a child theme, then include a file
							 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
							 */
							get_template_part( 'template-parts/content-news', get_post_format() );
			
						endwhile;
			
						the_posts_navigation();
			
					else :
			
						get_template_part( 'template-parts/content', 'none' );
			
					endif; ?>

					<?php
						
						if( have_rows('events') ):

						    while ( have_rows('events') ) : the_row();
						    	if(!get_sub_field('upcoming')) {
						    		$image = get_sub_field('image');
						?>
						    <li>
						    	<img src="<?php echo $image['url']; ?>">
						    	<h3><?php the_sub_field('title'); ?></h3>
						    	<small><?php the_sub_field('location'); ?></small>
						    </li>
						        
						<?php
								}
						    endwhile;
						
						else :
						
						    // no rows found
						
						endif;
					?>
				</div>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();