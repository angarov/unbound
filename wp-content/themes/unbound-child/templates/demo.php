<?php
/**
* Template Name: Demo
*
*/
get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<div class="container">
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

				<header class="title mid center" style="background-image: url(<?php echo get_the_post_thumbnail_url(get_the_ID()); ?>);">
					<h1><?php the_title(); ?></h1>
				</header>
				
	            <div class="row">
	                <div class="col-md-12">
		                <p class="max800 text-center">Complete the form below receive a free 30 day trial of Dyadic Distributed Key Protectionand Dyadic Software-Defined Encryption. Once the form is submitted, we’ll give you a call to set this up.</p>
	                </div>
	                <div class="col-md-12 form">
		                <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
						<script>
							hbspt.forms.create({
								css: '',
								portalId: '1761386',
								formId: 'cd1aa6ad-7c39-4f82-928d-3e97f414709e'
							});
						</script>
	                </div>
	            </div>
	            
				<?php endwhile; endif; ?>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();