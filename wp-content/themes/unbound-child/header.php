<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Unbound
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.12&appId=173210636044006&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'unbound' ); ?></a>

	<header id="masthead" class="site-header">
		<div class="dark-line">
			<div class="container">
				<div class="site-branding pull-left">
					<?php
					the_custom_logo();
					?>
				</div><!-- .site-branding -->
	
				<nav id="site-navigation" class="main-navigation">
					<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Primary Menu', 'unbound' ); ?></button>
					<?php
						wp_nav_menu( array(
							'theme_location' => 'menu-1',
							'menu_id'        => 'primary-menu',
						) );
					?>
				</nav><!-- #site-navigation -->
	
				<div class="search pull-right">
					<a href="<?php the_permalink(71); ?>" class="lets-talk">LET'S TALK</a>
					<a href="javascript:;" class="magnify"><img src="<?php echo get_template_directory_uri(); ?>-child/images/magnify.jpeg"></a>
					<?php //get_search_form(true); ?>
				</div>
			</div>
		</div>

		<div class="mega-holder">
			<div class="container">
				<div class="mega-menu">
					<div class="mega-item mega-projects">
						<?php show_type_box('project'); ?>
						<div class="book"><?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('navigation-box-sidebar', 'asdasd') ) :
 						endif; ?></div>
					</div>
					<div class="mega-item mega-usecase">
						<?php show_type_box('usecase'); ?>
						<div class="book"><?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('navigation-box-sidebar', 'asdasd') ) :
 						endif; ?></div>
					</div>
					<div class="mega-item mega-solutions">
						<?php show_type_box('solutions'); ?>
						<div class="book"><?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('navigation-box-sidebar', 'asdasd') ) :
 						endif; ?></div>
					</div>
					<div class="mega-item mega-company">
						<?php wp_nav_menu( array( 'theme_location' => 'company-menu', 'container_class' => 'company-menu-class' ) ); ?>
						<div class="book"><?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('navigation-box-sidebar', 'asdasd') ) :
 						endif; ?></div>
					</div>
				</div>
			</div>
		</div>
	</header><!-- #masthead -->

	<div id="content" class="site-content">
