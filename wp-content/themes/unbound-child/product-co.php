<?php
/**
* Template Name: Crypto-Orchestration
* Template Post Type: product
*/

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
		<?php while ( have_posts() ) : the_post(); ?>
		
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
	<header class="entry-header">
		<div class="container">
            <div class="row">
                <div class="col-md-6 pull-right">
					<h1>Unbound<br><?php echo get_the_title(); ?></h1>
                </div>
                <div class="col-md-6 pull-left">
					<img src="<?php echo get_template_directory_uri(); ?>-child/images/header1.png" class="header1">
                </div>
                <div class="col-md-6 pull-right">
					<h3>All-in-One Software Solution</h3>
					<p>A powerful combination of key management and protection, at the highest level of security</p>
					<a href="<?php the_permalink(71); ?>" class="cta">( Contact us )</a>
                </div>
            </div>
		</div>
	</header>
	
	<div class="container">
		<div class="tab-holder">
	        <div class="row tabs">
		        <div class="scroll">
		            <div class="col-md-3 tab selected" data-tab="overview">Overview</div>
		            <div class="col-md-3 tab" data-tab="features">Features</div>
		            <div class="col-md-3 tab" data-tab="deployment">Deployment options</div>
		            <div class="col-md-3 tab" data-tab="technical">Technical Specifications</div>
		        </div>
	        </div>
        </div>
	</div>
	
	<div class="tabs-content" id="tab-overview">

		<div class="all-in-one">
		    <div class="container">
		        <h2>Crypto Orchestration All in One</h2>
		        <p>With Unbound Crypto Orchestration you can, for the first time, control, manage, and protect distributed secrets across any infrastructure from one place.</p>
		        
		        <div class="boxes">
			        <div class="row">
				    	<div class="plus"><img src="<?php echo get_template_directory_uri(); ?>-child/images/plus.png"></div>
				    	<div class="col-md-6">
					    	<div class="box">
								<h3>Key management </h3>
								<p>Full key lifecycle management</p>
								<p>Manage any key - anywhere: on-premiseand private/public cloud</p>
								<p>Central management of keys and certificates</p>
							</div>
				    	</div>
				    	<div class="col-md-6">
					    	<div class="box">
								<h3>key protection</h3>
								<p>Hardware-Level Security</p>
								<p>Supports all standard crypto APIs</p>
								<p>Supports all standard keys and algorithms</p>
							</div>
				    	</div>
			        </div>
		        </div>
		    </div>
		</div>
		
	    <div class="stretching darker">
	    	<div class="container">
		    	<div class="row">
			    	<div class="col-md-3">
				    	<img src="<?php echo get_template_directory_uri(); ?>-child/images/lock1.png" class="ll">
			    	</div>
			    	<div class="col-md-6">
			            <h2>Stretching the Boundaries of Traditional Key Protection</h2>
			            <p>Unbound Key Control ensures that your most sensitive keys <b>never exist in the clear at any point in their lifecycle</b> - not even when generated,while in use or while at rest. With Unbound, key material is never whole,not in memory, disk or network.</p>
			
						<p>By eliminating this single point of failure, Unbound Key Control can <b>stretch the secure boundary far beyond the traditional physical casing.</b></p>
					    <a href="<?php the_permalink(17); ?>" class="cta darker">( Learn How )</a>
			    	</div>
			    	<div class="col-md-3">
				    	<img src="<?php echo get_template_directory_uri(); ?>-child/images/lock2.png" class="rr">
			    	</div>
		    	</div>
	    	</div>
	    </div>
		
	    <div class="silos">
	    	<div class="container">
		    	<h2>No More Silos - One System to Manage Them All</h2>
		    	<p>Unbound's combined solution provides full key lifecycle management from generation, to usage, revocation, rotation, and backup. It supports all standard HSM crypto APIs and enables seamless integration with all KM systems. This pure software solution protects and manages all keys from all on-premises or cloud workloads and from any cloud service provider (CSP). <b>Use Unbound Key Control to manage and sync all your keys across sites and workloads through one central management system.</b></p>
				<img src="<?php echo get_template_directory_uri(); ?>-child/images/map1.png">
	    	</div>
	    </div>
	        
		<div class="darker-row darker">
		    <div class="container">
		        <h2>Empower the needs of SecOpsin<br>in Any Organization</h2>
		        <p>Unbound empowers the SecOps team with a fully-outfitted infrastructure for highly efficient key management and protection. This all-in-one solution saves SecOps the time and effort of integrating multiple products, by combining every critical feature to the SecOp workflow, including: granular policy enforcement, monitoring and auditing, resource management, administration and configuration of role-based access control, backup and durability.</p>
		    </div>
		</div>
	    
	    <div class="do-for-you for-project">
	    	<div class="container max1000">
	            <h2>What's Made Possible with Unbound Key Control?</h2>
				<div class="row">
				<?php
					//$recent_posts = get_posts( array( 'offset' => 0, 'numberposts' => '4', 'post_status' => 'publish', 'post_type' => 'usecase' ) );
					$args = array( 
						'post_type' => 'usecase',
						'post_status' => 'publish',
						'category__in' => array(60),
					);
					$recent_posts = query_posts( $args, ARRAY_A );
					wp_reset_query();
	
					foreach( $recent_posts as $recent ):
						$image = wp_get_attachment_image_src( get_post_thumbnail_id( $recent->ID ) );
				?>
					<div class="col-md-4">
						<div class="box">
							<a href="<?php echo get_permalink($recent->ID); ?>">
								<img src="<?php echo $image[0]; ?>">
								<div class="a1"><p><?php echo $recent->post_title; ?></p></div><div class="a2"><?php echo get_field('preview_box', $recent->ID); ?></div>
								<span class="more">Read More>>></span>
							</a>
							<div class="red-bottom"></div>
						</div>
					</div>
				<?php endforeach; ?>
				</div>
	        </div>
	    </div>
		
	    <div class="what-to">
	    	<div class="container">
		    	<h2>What Would You Like to Unbind?</h2>
		    	<?php echo do_shortcode("[unbind cat=58]" ) ?>
		    </div>
	    </div>
	</div>
	
	<div class="tabs-content" id="tab-features">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<h2>Elastic and Scalable</h2>
					<p>Fully elastic and scalable enterprise key management lets you quickly adapt to meet your changing needs. Stay up to date and running the latest crypto, with update cycles measured in days.</p>
				</div>
				<div class="col-md-6">
					<div class="image"><img src="<?php echo get_template_directory_uri(); ?>-child/images/f0.png"></div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 pull-right">
					<div class="image"><img src="<?php echo get_template_directory_uri(); ?>-child/images/f1.png"></div>
				</div>
				<div class="col-md-6">
					<h2>Transparent and Seamless Integration</h2>
					<p>Completely transparent and easily deployed without disrupting the existing workflow of applications. Support all industry standard HSM and Key Management APIs, as well as all standard crypto algorithms.</p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="image"><img src="<?php echo get_template_directory_uri(); ?>-child/images/f2.png"></div>
				</div>
				<div class="col-md-6">
					<h2>Easy Operation and Automation</h2>
					<p>With CLI and REST APIs included, you can fully automate system installation, deployment, ongoing operation, and management, saving you and your team from spending precious time on labor-intensive tasks.</p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 pull-right">
					<div class="image"><img src="<?php echo get_template_directory_uri(); ?>-child/images/f3.png"></div>
				</div>
				<div class="col-md-6">
					<h2>Future-ready and Agile Cryptography</h2>
					<p>Unbound Crypto Orchestration is future-ready, so your cryptography infrastructure can be too.<br>
	With the emergence of Quantum Computing, Blockchain, and crypto vulnerabilities, changes in crypto are faster than ever. Unbound provide a crypto-agile system that ensures you will be up and running the latest crypto, with update cycles measured in days to weeks, not months or years.</p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="image"><img src="<?php echo get_template_directory_uri(); ?>-child/images/f4.png"></div>
				</div>
				<div class="col-md-6">
					<h2>Secured Management and Administration</h2>
					<p>Unbound vHSM allows you to customize granular admin authorization and access management policies in multiple ways. For example, you can define a minimum number of admins who must work in unison on high-security operations. Or add another layer of access security at the application level, in addition to server authorization.</p>
				</div>
			</div>
			<div class="row noborder">
				<div class="col-md-6 pull-right">
					<div class="image"><img src="<?php echo get_template_directory_uri(); ?>-child/images/f5.png"></div>
				</div>
				<div class="col-md-6">
					<h2>Context-Based Auditing</h2>
					<p>Get the full details of every decrypt or signing operation whenever a key is used. Receive detailed logs that include: operation type, date and time, the servers from which the request was made, and the authorizing users. View the audit logs in the Unbound vHSM console or export them to a third-party tool such as a SIEM.</p>
				</div>
			</div>
		</div>
	</div>
	
	<div class="tabs-content" id="tab-deployment">
		<div class="container">
			<h2>Architecture - Non-continuous Secure Boundary </h2>
			<p>Each Unbound Orchestration is comprised of one or more pairs of standard servers that are installed and managed by the customer. Each of these pairs is comprised of an Entry Point node and a Partner node that each hold one share of a key. Together, these servers form the secure boundary of Unbound Orchestration. Application servers within the network connect to the entry point for consuming cryptographic services for the keys that are managed within the Orchestration.</p>
			
			<img src="<?php echo get_template_directory_uri(); ?>-child/images/map2.png" class="map2">
			
			<b class="pp1">The EKM limitless Secure Boundary adds a newly created dimension to security architectures, creating endless options for separation of the EKM nodes such as:</b>
			
			<div class="ekm">
				<div class="row">
					<div class="col-md-4">
						<div class="box">
							<div class="wrap">
								<img src="<?php echo get_template_directory_uri(); ?>-child/images/e1.png">
								<p>Separate locations/entities, e.g. networks, geographical locations, cloud availability zones etc.</p>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="box">
							<div class="wrap">
								<img src="<?php echo get_template_directory_uri(); ?>-child/images/e2.png">
								<p>Separate credentials and access controls</p>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="box">
							<div class="wrap">
								<img src="<?php echo get_template_directory_uri(); ?>-child/images/e3.png">
								<p>Separate software stacks (e.g. different operating systems)</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		   
		<div class="onprem darker darker-row">
		    <div class="container">
		        <h2>Key Management for Cloud,<br>On-premises and Hybrid Environments </h2>
		        <p>Based on the first technology to truly abstract key management, Unbound Orchestration can be deployed on any standard platform, including physical/virtual machines and containers. This gives you the flexibility to choose the location of the nodes for the Orchestration and to create a deployment that meets your unique requirements. </p>
		        <p><img src="<?php echo get_template_directory_uri(); ?>-child/images/map3.png" class="map3 only-desktop"></p>
		        <p><img src="<?php echo get_template_directory_uri(); ?>-child/images/map3m.png" class="map3 only-mobile"></p>
		    </div>
		</div>

		<div class="automation">
			<div class="container">
				<h2>Transparent Integration & Automation of the Key Management Infrastructure</h2>
				<p>Unbound Orchestration can be deployed easily without disrupting the existing workflow of applications. </p>
				
				<div class="ekm">
					<div class="row">
						<div class="col-md-4">
							<div class="box">
								<div class="wrap">
									<img src="<?php echo get_template_directory_uri(); ?>-child/images/e6.png">
									<p>Supports full key lifecycle management </p>
								</div>
								<div class="red-bottom"></div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="box">
								<div class="wrap">
									<img src="<?php echo get_template_directory_uri(); ?>-child/images/e4.png">
									<p>Fully transparent to the calling application and supports all crypto APIs </p>
								</div>
								<div class="red-bottom"></div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="box">
								<div class="wrap">
									<img src="<?php echo get_template_directory_uri(); ?>-child/images/e5.png">
									<p>Allow full automation using CLI and REST APIs</p>
								</div>
								<div class="red-bottom"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>
	
	<div class="tabs-content" id="tab-technical">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2><img src="<?php echo get_template_directory_uri(); ?>-child/images/t0.png" class="map2">Operating Systems and Platform</h2>
					<ul>
						<li>Windows, Linux</li>
						<li>Any standard virtual/physical machine</li>
						<li>Cloud IaaS: All cloud service providers including AWS, Azure, Google Cloud Platform, SoftLayer</li>
						<li>PaaS and Containers: Docker, Kuberentes, Pivotal Cloud Foundry</li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h2><img src="<?php echo get_template_directory_uri(); ?>-child/images/t1.png" class="map2">API Support</h2>
					<ul>
						<li>PKCS#11, Java (JCE) Microsoft CNG, OpenSSL, REST</li>
						<li>KMIP server providing KMIP services to any KMIP client up to KMIP 1.3 inclusive</li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h2><img src="<?php echo get_template_directory_uri(); ?>-child/images/t2.png" class="map2">Cryptography</h2>
					<ul>
						<li>Full Suite B support</li>
						<li>Asymmetric: RSA (key sizes: 2048, 3072, 4096; modes: RAW, PKCS1, PSS, OAEP), Elliptic Curve Cryptography with P256 | P384 | P521 curves</li>
						<li>Symmetric: AES (key sizes: 128, 256; modes: SIV, XTS, ECB, CBC, OFB, CFB, CTR, CCM, GCM, NIST_WRAP, CMAC, GMAC), Triple DES (key size: 168; modes: ECB, CBC, OFB, CFB, CTR) 
Hash/HMAC: SHA-256, SHA-384</li>
						<li>Generic secret management</li>
						<li>Additional modules: Application level encryption, password verification, Post-Quantum Crypto (PQC), Bitcoin and blockchain</li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h2><img src="<?php echo get_template_directory_uri(); ?>-child/images/t3.png" class="map2">Client Authentication</h2>
					<ul>
						<li>Server level authentication: using client certificate, mutually authenticated TLS 1.2</li>
						<li>Application level authentication (optional): SAML Authentication Scheme, Active Directory </li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h2><img src="<?php echo get_template_directory_uri(); ?>-child/images/t4.png" class="map2">High Availability</h2>
					<ul>
						<li>Active/Active and Active/Passive modes</li>
						<li>Automated load balancing by EKM Client</li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h2><img src="<?php echo get_template_directory_uri(); ?>-child/images/t5.png" class="map2">Management and administration</h2>
					<ul>
						<li>Admin Console via Web UI</li>
						<li>Command Line Interface (CLI)</li>
						<li>Full management REST API</li>
						<li>Full backup and restore functionality, no additional devices required </li>
						<li>Highly configurable Role Based Access Control (RBAC) model</li>
						<li>Multi-admin and quorum authentication - supported remotely over LAN/WAN, no physical access is necessary </li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h2><img src="<?php echo get_template_directory_uri(); ?>-child/images/t6.png" class="map2">Performance Specifications</h2>
					<ul>	
						<li>Cryptographically isolated partitions: up to 100,000,000</li>
						<li>Keys: Virtually unlimited, bound by disk space only</li>
						<li>Simultaneous connected hosts: up to 20,000</li>
						<li>Capacity in transactions per second (TPS) 3 for sample configurations</li>
					</ul>
					<table class="with-pp">
						<tr>
							<th class="gray"></th>
							<th class="gray pp-start">Basic Unit</th>
							<th class="gray">Sample 'S' Cluster</th>
							<th class="gray">Sample 'M' Cluster</th>
							<th class="gray">Sample 'L' Cluster</th>
						</tr>
						<tr>
							<td class="gray remove-bt"></td>
							<td class="pp">1 pair of servers,<br>1 core per server</td>
							<td>1 pair of servers,<br>2 cores per server</td>
							<td>2 pairs of servers,<br>4 cores per server</td>
							<td>4 pairs of servers,<br>8 cores per server</td>
						</tr>
						<tr>
							<td class="gray">RSA-2048</td>
							<td class="pp">100</td>
							<td>200</td>
							<td>800</td>
							<td>3200</td>
						</tr>
						<tr>
							<td class="gray">ECIES P256 </td>
							<td class="pp">100</td>
							<td>200</td>
							<td>800</td>
							<td>3200</td>
						</tr>
						<tr>
							<td class="gray">AES-GCM 128 single block</td>
							<td class="pp-end">200 <img src="<?php echo get_template_directory_uri(); ?>-child/images/arrow1.png" class="map2"></td>
							<td>400 <img src="<?php echo get_template_directory_uri(); ?>-child/images/arrow1.png" class="map2"></td>
							<td>1600 <img src="<?php echo get_template_directory_uri(); ?>-child/images/arrow1.png" class="map2"></td>
							<td>6400 <img src="<?php echo get_template_directory_uri(); ?>-child/images/arrow1.png" class="map2"></td>
						</tr>
						
					</table>
					<p class="cpu">
						Capacity is derived from the number of CPU cores in the EKM cluster. Scaling the Basic EKM Unit is done by scaling up or scaling out, and is fully linear, as illustrated in the sample clusters above
					</p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h2><img src="<?php echo get_template_directory_uri(); ?>-child/images/t7.png" class="map2">Security Certifications</h2>
					<ul>
						<li>FIPS 140-2 (in process)</li>
						<li>Common Criteria (in process)</li>
					</ul>
				</div>
			</div>
		</div>
		</div>
	</div>
      
	<div class="darker-row darker">
	    <div class="container">
	        <h2>Protect and Manage Security Keys with Unbound Key Control</h2>
		    <a href="<?php the_permalink(71); ?>" class="cta darker">( Let's talk )</a>
	    </div>
	</div>

	<?php echo do_shortcode("[articals_list limit=4]"); ?>
	
</article><!-- #post-<?php the_ID(); ?> -->

		<?php endwhile; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();