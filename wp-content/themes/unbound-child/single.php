<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Unbound
 */

get_header(); ?>

	<div class="container">
		<div id="primary" class="content-area">
			<div class="container">
				<main id="main" class="site-main">
				<?php
				while ( have_posts() ) : the_post();
		
					get_template_part( 'template-parts/content', get_post_type() );
		
	// 				the_post_navigation();
		
				endwhile; // End of the loop.
				?>
		
				</main><!-- #main -->
			</div>
		</div><!-- #primary -->
		<?php get_sidebar(); ?>
		
	</div>
	<div class="mc4wp_form">
		<h2>Subscribe to BLOG</h2>
			<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
			<script>
				hbspt.forms.create({
					css: '',
					portalId: '1761386',
					formId: '03267eb1-b6a9-4d19-807a-a44f33bbff76'
				});
			</script>
		</div>
	<?php if ( function_exists( 'echo_crp' ) ) echo_crp(); ?>
<?php
get_footer();
