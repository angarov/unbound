<?php
/**
* Template Name: Events
*
*/
get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<div class="container">
				<header class="title mid center" style="background-image: url(<?php echo get_the_post_thumbnail_url(get_the_ID()); ?>);">
		            <h1>Events</h1>
				</header>
				
				<div class="partners events">
					
					<ul class="partners-list">
					<?php
						
						
						$args = array(
							'numberposts' => -1,
							'orderby' => 'post_date',
							'order' => 'DESC',
							'post_type' => 'event',
							'post_status' => 'publish'
						);
						$recent_posts = wp_get_recent_posts( $args, ARRAY_A );
						
						foreach ( $recent_posts as $post ) :
					    	if(get_field('upcoming', $post['ID'])) {
					    		$featured_image_url = wp_get_attachment_url( get_post_thumbnail_id( $post['ID'] ) );
					    		
					    		$link = "#";
					    		if(($url = get_field('link', $post['ID'])) != "" ) {
						    		$link = $url;
					    		}
					    		
					    		$link2 = "#";
					    		if(($url = get_field('link2', $post['ID'])) != "" ) {
						    		$link2 = $url;
					    		} else {
						    		$link2 = '/company/contact-us/';
					    		}
					?>
					    <li>
					    	<div class="logo" style="background-color:<?php echo get_field('background', $post["ID"]); ?>"><a href="<?php echo $link; ?>" target="<?php echo $link == '#' ? '_self' : '_blank'; ?>"><img src="<?php echo $featured_image_url; ?>"></a></div>
					    	<small><?php the_field('location', $post['ID']); ?></small>
					    	<h2><a href="<?php echo $link; ?>" target="<?php echo $link == '#' ? '_self' : '_blank'; ?>"><?php echo $post['post_title']; ?></a></h2>
					    	<p><?php echo $post['post_content']; ?></p>
					    	<?php if(($link = get_field('link', $post['ID'])) != "" ): ?>
					    	<a href="<?php echo $link2; ?>" class="cta">( Schedule a meeting )</a>
					    	<?php endif; ?>
					    </li>
					        
					<?php
							}
					    endforeach;
					?>
					</ul>
				</div>
			</div>
			
			<div class="past-events">
				<div class="container">
					<ul>
						<li><h2>Past Events</h2></li>
					<?php
						
						foreach ( $recent_posts as $post ) :
					    	if(!get_field('upcoming', $post['ID'])) {
					    	$featured_image_url = wp_get_attachment_url( get_post_thumbnail_id( $post['ID'] ) );
					?>
					    <li>
					    	<img src="<?php echo $featured_image_url; ?>">
					    	<h3><?php echo $post['post_title']; ?></h3>
					    	<small><?php the_field('location', $post['ID']); ?></small>
					    </li>
				        
					<?php
							}
					    endforeach;
					?>
					</ul>
				</div>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();