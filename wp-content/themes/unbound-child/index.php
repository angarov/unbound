<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Unbound
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<div class="container">

				<header class="title mid center" style="background-image: url(/wp-content/uploads/2017/12/image.jpg);">
					<h1>Unbound Blog</h1>
				</header>
				<div class="news-slider" data-index="0">
		<?php
		if ( have_posts() ) :
			$isFirst = true;
			/* Start the Loop */
			
			$the_query = new WP_Query( array( 'meta_key' => '_is_ns_featured_post', 'meta_value' => 'yes' ) );
			if ( $the_query->have_posts() ) {
				while ( $the_query->have_posts() ) {
					$the_query->the_post();
					get_template_part( 'template-parts/content', 'first' );
				}
				wp_reset_postdata();
			} else {
			}
			wp_reset_postdata();
		?>
					<div class="pagination"><a href="javascript:;" data-dir="1"><</a> <span></span> <a href="javascript:;" data-dir="-1">></a></div>
				</div>
		<div class="posts">
		<?php
			while ( have_posts() ) : the_post();
				get_template_part( 'template-parts/content', get_post_format() );

			endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );
			get_sidebar();

		endif; ?>
				</div>
				<div class="sidebar"><?php get_sidebar(); ?></div>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->
		<div class="mc4wp_form">
			<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
			<script>
				hbspt.forms.create({
					css: '',
					portalId: '1761386',
					formId: '03267eb1-b6a9-4d19-807a-a44f33bbff76'
				});
			</script>
		</div>

<?php
get_footer();
