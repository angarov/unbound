<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Unbound
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
		<div class="container">
			<div class="row">
			  <div class="col-md-3">
					<h3>CONTACT INFO</h3>
					
					<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("footerarea'") ) : ?>
					<?php endif;?>


					<!--<div class="big-icons">
						<div class="row">
							<div class="col-md-5">
								<a href="<?php echo get_page_link(69); ?>"><img src="<?php echo get_template_directory_uri(); ?>-child/images/blog.png">BLOG</a>
							</div>
							<div class="col-md-7">
								<a href="<?php echo get_page_link(28); ?>"><img src="<?php echo get_template_directory_uri(); ?>-child/images/labs.png">Unbound Labs</a>
							</div>
						</div>
					</div>-->

					<div class="newsletter">
						<!--<h4>SIGN UP FOR NEWSLETTER</h4>
						<form>
							<input type="email" placeholder="name@example.com">
							<input type="submit" value="Submit">
						</form>-->
						<!--[if lte IE 8]>
						<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
						<![endif]-->
						<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
						<script>
							hbspt.forms.create({
								css: '',
								portalId: '1761386',
								formId: '03267eb1-b6a9-4d19-807a-a44f33bbff76'
							});
						</script>
					</div>
				</div>
				<div class="col-md-9">
					<div class="row top40">
						<div class="col-md-3">
							<h4>PRODUCTS</h4>
							<?php show_type_box('product'); ?>
						</div>
						<div class="col-md-4">
							<h4>Use cases</h4>
							<?php show_type_box('usecase'); ?>
						</div>
						<div class="col-md-3">
							<h4>Solutions</h4>
							<?php show_type_box('solutions'); ?>
						</div>
						<div class="col-md-2">
							<h4><a href="<?php echo get_page_link(17); ?>">TECHNOLOGY</a></h4>
							<h4><a href="<?php echo get_page_link(19); ?>">RESOURCES</a></h4>
							<h4><a href="javascript:;">COMPANY</a></h4>
							<?php wp_nav_menu( array( 'theme_location' => 'company-menu', 'container_class' => 'company-menu-class' ) ); ?>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="closer">
					© Unbound Tech  |  <a href="<?php echo get_page_link(8040); ?>">Privacy</a>  |  <a href="<?php echo get_page_link(8043); ?>">Terms</a>  |  <a href="<?php echo get_page_link(8569); ?>">Cookies</a>
				</div>
			</div>
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->
<div class="search-window">
	<div class="search-form"><?php get_search_form(true); ?></div>
</div>

<?php wp_footer(); ?>

<div class="inner-popup">
	<div class="popup-content">
		<a href="javascript:;" class="close-popup">X</a>
		<div class="block">
			<?php echo get_post_content(9554); ?>
		</div>
	</div>
	<div class="overlay"></div>
</div>
</body>
</html>
