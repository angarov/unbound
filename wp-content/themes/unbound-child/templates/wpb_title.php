<?php
/**
* Template Name: WPB + Title
*
*/
get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<div class="container">
		<?php
		while ( have_posts() ) : the_post();
		?>
			<header class="title mid center" style="background-image: url(<?php echo get_the_post_thumbnail_url(get_the_ID()); ?>);">
				<h1><?php the_title(); ?></h1>
			</header>
	            
	    <?php

			the_content();

		endwhile; // End of the loop.
		?>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
