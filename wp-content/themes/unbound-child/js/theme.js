var tim = null;

jQuery(function($) {
    
    jQuery('.news-slider .pagination span').html('1 of ' + jQuery('.news-slider article').get().length);
    jQuery('.news-slider .pagination a').click(function(){
	    var dir = parseInt(jQuery(this).data('dir'));
	    var index = parseInt(jQuery('.news-slider').data('index')) + dir;
	    index %= jQuery('.news-slider article').get().length;
	    if( index < 0 ) { index = jQuery('.news-slider article').get().length - 1; }
	    jQuery('.news-slider').data('index', index);
		jQuery('.news-slider .pagination span').html( (index+1) + ' of ' + jQuery('.news-slider article').get().length);
    
	    jQuery('.news-slider article').fadeOut(function(){
		    jQuery('.news-slider article:eq('+index+')').fadeIn();
	    });
    });
    
    jQuery('.magnify').click(function() {
		jQuery('.search-window').fadeIn();
		jQuery('.search-window input[type="search"]').focus();
	});
    jQuery('.search-window').click(function() {
		jQuery('.search-window').fadeOut();
	});
    jQuery('.search-window input').click(function( event ) {
		event.stopPropagation();
	});
	
	

	jQuery('.tabs .tab').click(function() {
		show_tab(jQuery(this).data('tab'));
		window.location.hash = jQuery(this).data('tab');
    });
    
    var hash = window.location.hash.replace("#","");
    
    if(hash != "") {
	    show_tab(hash);
    } else {
		show_tab('overview');
    }
    
    if( window.location.href.indexOf('ref=dyadicsec') >= 0 ) {
	    open_login();
    }
	jQuery( ".close-popup, .entry-popup a, .overlay" ).click(function(){
	    open_login();
    });
})

function open_login() {
	jQuery('.inner-popup').fadeToggle();
	jQuery('.popup-content').toggleClass("flyin");
}

function show_tab(tab) {
	jQuery('.tabs .tab').removeClass('selected');
	jQuery('.tabs .tab.tab-' + tab).addClass('selected');
	
	jQuery('.features, .overview, .deployment, .technical').hide();
	jQuery('.' + tab).show();
}