<?php
/**
* Template Name: Resources
*
*/
get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			<div class="container">
				<header class="title mid center" style="background-image: url(<?php echo get_the_post_thumbnail_url(get_the_ID()); ?>);">
					<h1><?php the_title(); ?></h1>
				</header>
				<div class="sun-tabs-resources">
					<ul class="sun-tabs-resources-float">
						<li><a href="#Upcoming">Upcoming Webinars</a></li>
						<li><a href="#On-demand">On-demand Webinars</a></li>
						<li><a href="#Whitepapers">Whitepapers</a></li>
						<li><a href="#Datasheets">Datasheets</a></li>
						<li><a href="#Videos">Videos</a></li>
					</ul>
				</div>
			</div>
	        <?php the_content(); ?>
	            
			<?php endwhile; endif; ?>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();