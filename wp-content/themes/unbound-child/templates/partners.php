<?php
/**
* Template Name: Partners
*
*/
get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<div class="container">
				
				<header class="title mid center text-center" style="background-image: url(/wp-content/uploads/2017/12/image-1.jpg);">
		            <h1><?php the_title(); ?></h1>
		            <p><?php the_field('subtitle'); ?></p>
		            <a href="<?php the_field('become_a_partner'); ?>">( Become a Partner )</a>
				</header>
				
				<div class="partners">
					<div class="intro-min">
						<h2><?php the_field('title2'); ?></h2>
						<p><?php the_field('subtitle2'); ?></p>
					</div>
					
					<ul class="partners-list">
					<?php
						
						if( have_rows('partner') ):

						    while ( have_rows('partner') ) : the_row();
						    	$image = get_sub_field('logo');
						?>
						    <li>
						    	<div class="logo" style="background-color: <?php echo get_sub_field('background') == 'Gray' ? '#e2e2e2' : '#fff'; ?>"><img src="<?php echo $image['url']; ?>"></div>
						    	<p><?php the_sub_field('description'); ?></p>
						    </li>
						        
						<?php
						    endwhile;
						
						else :
						
						    // no rows found
						
						endif;
					?>
					    <li class="become">
					    	<h3><?php the_field('join_text'); ?></h3>
					    	<a href="<?php the_field('become_a_partner'); ?>" class="cta">( Become a Partner )</a>
					    </li>
					</ul>
				</div>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();