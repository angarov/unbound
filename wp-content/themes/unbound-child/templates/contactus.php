<?php
/**
* Template Name: Contact Us
*
*/
get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<div class="container">
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

				<header class="title mid center" style="background-image: url(<?php echo get_the_post_thumbnail_url(get_the_ID()); ?>);">
					<h1><?php the_title(); ?></h1>
				</header>
				
	            <div class="row">
	                <div class="col-md-6">
		                <h2>Send us a message!</h2>
		                <?php //the_content(); ?>
		                <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
						<script>
							hbspt.forms.create({
								css: '',
								portalId: '1761386',
								formId: 'fefdb6eb-a354-45e1-9f57-527feed42ffa'
							});
						</script>
	                </div>
	                <div class="col-md-6">
		                <h2>Contact us</h2>
		                <p>Great to hear from you! We’d love to answer all your questions. Tell us how to reach you, and a Dyadic representative will get back to you as soon as possible.</p>
		                
		                
			            <div class="row info">
			                <div class="col-md-6">
				                <p><img src="<?php echo get_template_directory_uri(); ?>-child/images/pin.png"><b>New York</b>10 E 53rd Street, 14th Floor New York, NY 10022</p>
				                <p><img src="<?php echo get_template_directory_uri(); ?>-child/images/tel.png">631-316-8114</p>
				                <p><img src="<?php echo get_template_directory_uri(); ?>-child/images/email.png">contact@dyadicsec.com</p>
			                </div>
			                <div class="col-md-6">
				                <p><img src="<?php echo get_template_directory_uri(); ?>-child/images/pin.png"><b>Israel</b>25 Efal st. Beit Amot Platinum Petach Tikva 4951125</p>
				                <p><img src="<?php echo get_template_directory_uri(); ?>-child/images/tel.png">+972-72-277-3437</p>
			                </div>
			            </div>
	            
	                </div>
	            </div>
	            
				<?php endwhile; endif; ?>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();