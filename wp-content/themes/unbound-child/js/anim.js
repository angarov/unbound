function resize_tabs() {
	jQuery(".vc_tta-tabs-list").each(function( index ) {
        var li = jQuery(this).find('.vc_tta-tab').get();
        jQuery(this).find('.vc_tta-tab').width( jQuery(this).width() / li.length );
	});
}

jQuery(function($) {
	
	var isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
	if (!isMobile) {
		var controller = jQuery.superscrollorama({ triggerAtCenter: true, playoutAnimations: false});
	    
	    jQuery('a.cta').each(function( index ) {
	        var html = jQuery(this).html();
	        var isDark = jQuery(this).attr('class').indexOf('darker') > -1;
	        html = html.replace("(", "<div class=\"cc l\"><img src=\"" + unconf.templateUrl + "/images/" + (isDark ? "cl2.svg" : "cl.svg") + "\"></div><div class=\"inner\">");
	        html = html.replace(")", "</div><div class=\"cc r\"><img src=\"" + unconf.templateUrl + "/images/" + (isDark ? "cr2.svg" : "cr.svg") + "\"></div>");
	        jQuery(this).html(html);
	        jQuery(this).show();
	        
	        jQuery(this).mouseover(function(){
		        if( jQuery(this).data('status') != 'on' ) {
			        jQuery(this).data('status', 'on');
			        jQuery(this).find('.cc').animate({width:'51%'}, 400, function(){
				        jQuery(this).animate({width:'15px'}, 400);
			        });
		        }
	        }).mouseleave(function(){
			    jQuery(this).data('status', 'off');
		    });
		});

		if(jQuery('.sun-tabs-resources')[0]) {
			jQuery(window).scroll(function() {
			    if(jQuery(window).scrollTop() > jQuery('.sun-tabs-resources').offset().top - 120) {
				   jQuery('.sun-tabs-resources').addClass('sticky'); 
			    } else {
				   jQuery('.sun-tabs-resources').removeClass('sticky'); 
				}
			});
		}
		if(jQuery('.single-product')[0]) {
			jQuery(window).scroll(function() {
			    if(jQuery(window).scrollTop() > jQuery('.tab-holder').offset().top - 120) {
				   jQuery('.tab-holder').addClass('sticky'); 
			    } else {
				   jQuery('.tab-holder').removeClass('sticky'); 
				}
			});
		}
		
		jQuery(window).resize(function() {
		    resize_tabs();
		});
		resize_tabs();
        
        
	        
		/*controller.addTween('.suite h3', (new TimelineLite())
		.append([TweenMax.from(jQuery('.suite h3'), .5, {css:{opacity: 0}, immediateRender:true}, {css:{opacity: 1}})])
		.append([TweenMax.from(jQuery('.suite .box img'), .5, {css:{opacity: 0}, immediateRender:true}, {css:{opacity: 1}})]), 0, -200);
		
		controller.addTween('.hardware .icon1', (new TimelineLite())
		.append([TweenMax.from(jQuery('.hardware .col-md-4:eq(0)'), .5, {css:{opacity: 0}, immediateRender:true}, {css:{opacity: 1}})])
		.append([TweenMax.from(jQuery('.hardware .col-md-4:eq(1)'), .5, {css:{opacity: 0}, immediateRender:true}, {css:{opacity: 1}})])
		.append([TweenMax.from(jQuery('.hardware .col-md-4:eq(2)'), .5, {css:{opacity: 0}, immediateRender:true}, {css:{opacity: 1}})]), 0, -200);
		
		controller.addTween('.testimonials .mid', TweenMax.from($('.testimonials .mid'), .5, {css:{opacity:0}}), 0, -200);
		
		
		//****************************
		
		controller.addTween('.stretching', TweenMax.from($('.stretching .ll'), .5, {css:{opacity:0,left:'-50px'}}), 0, 200);
		controller.addTween('.stretching', TweenMax.from($('.stretching .rr'), .5, {css:{opacity:0,right:'-50px'}}), 0, 200);
		
		controller.addTween('.silos', TweenMax.from($('.silos img'), .5, {css:{opacity:0,top:'50px'}}), 0, 200);
		
		jQuery('#tab-features .row').each(function( index ) {
	        var isRev = jQuery(this).find('.col-md-6:eq(0)').attr('class').indexOf('pull-right') > -1;
			controller.addTween(jQuery(this), TweenMax.from(jQuery(this).find(isRev ? '.col-md-6:eq(1)' : '.col-md-6:eq(0)'), .5, {css:{opacity:0,left:'-50px'}}));
			controller.addTween(jQuery(this), TweenMax.from(jQuery(this).find(isRev ? '.col-md-6:eq(0)' : '.col-md-6:eq(1)'), .5, {css:{opacity:0,right:'-50px'}}));
		});
		
		controller.addTween('.ekm:eq(0)', (new TimelineLite())
		.append([TweenMax.from(jQuery('.ekm .col-md-4:eq(0) img'), .5, {css:{opacity: 0,top:'10px'}, immediateRender:true}, {css:{opacity: 1}})])
		.append([TweenMax.from(jQuery('.ekm .col-md-4:eq(1) img'), .5, {css:{opacity: 0,top:'10px'}, immediateRender:true}, {css:{opacity: 1}})])
		.append([TweenMax.from(jQuery('.ekm .col-md-4:eq(2) img'), .5, {css:{opacity: 0,top:'10px'}, immediateRender:true}, {css:{opacity: 1}})]), 0, 0);
		
		
		controller.addTween('.automation .ekm', (new TimelineLite())
		.append([TweenMax.from(jQuery('.automation .ekm .col-md-4:eq(0) img'), .5, {css:{opacity: 0,top:'10px'}, immediateRender:true}, {css:{opacity: 1}})])
		.append([TweenMax.from(jQuery('.automation .ekm .col-md-4:eq(1) img'), .5, {css:{opacity: 0,top:'10px'}, immediateRender:true}, {css:{opacity: 1}})])
		.append([TweenMax.from(jQuery('.automation .ekm .col-md-4:eq(2) img'), .5, {css:{opacity: 0,top:'10px'}, immediateRender:true}, {css:{opacity: 1}})]), 0, 0);
		
		
		//****************************
		
		
		controller.addTween('.mathematical', TweenMax.from($('.mathematical .l'), .5, {css:{opacity:0,left:'10px'}}), 0, 200);
		controller.addTween('.mathematical', TweenMax.from($('.mathematical .r'), .5, {css:{opacity:0,right:'10px'}}), 0, 200);
		
		controller.addTween('.vhsm .box', (new TimelineLite())
		.append([TweenMax.from(jQuery('.vhsm .box:eq(0) img'), .5, {css:{opacity: 0,top:'10px'}, immediateRender:true}, {css:{opacity: 1}})])
		.append([TweenMax.from(jQuery('.vhsm .box:eq(1) img'), .5, {css:{opacity: 0,top:'10px'}, immediateRender:true}, {css:{opacity: 1}})])
		.append([TweenMax.from(jQuery('.vhsm .box:eq(2) img'), .5, {css:{opacity: 0,top:'10px'}, immediateRender:true}, {css:{opacity: 1}})])
		.append([TweenMax.from(jQuery('.vhsm .box:eq(3) img'), .5, {css:{opacity: 0,top:'10px'}, immediateRender:true}, {css:{opacity: 1}})]), 0, 0);
		
		
		//****************************
		
		
		controller.addTween('.centralized .box', (new TimelineLite())
		.append([TweenMax.from(jQuery('.centralized .box:eq(0) img'), .5, {css:{opacity: 0,top:'10px'}, immediateRender:true}, {css:{opacity: 1}})])
		.append([TweenMax.from(jQuery('.centralized .box:eq(1) img'), .5, {css:{opacity: 0,top:'10px'}, immediateRender:true}, {css:{opacity: 1}})])
		.append([TweenMax.from(jQuery('.centralized .box:eq(2) img'), .5, {css:{opacity: 0,top:'10px'}, immediateRender:true}, {css:{opacity: 1}})]), 0, 0);
		
		controller.addTween('.streamlined .box', (new TimelineLite())
		.append([TweenMax.from(jQuery('.streamlined .box:eq(0) img'), .5, {css:{opacity: 0,top:'10px'}, immediateRender:true}, {css:{opacity: 1}})])
		.append([TweenMax.from(jQuery('.streamlined .box:eq(1) img'), .5, {css:{opacity: 0,top:'10px'}, immediateRender:true}, {css:{opacity: 1}})])
		.append([TweenMax.from(jQuery('.streamlined .box:eq(2) img'), .5, {css:{opacity: 0,top:'10px'}, immediateRender:true}, {css:{opacity: 1}})])
		.append([TweenMax.from(jQuery('.streamlined .box:eq(3) img'), .5, {css:{opacity: 0,top:'10px'}, immediateRender:true}, {css:{opacity: 1}})])
		.append([TweenMax.from(jQuery('.streamlined .box:eq(4) img'), .5, {css:{opacity: 0,top:'10px'}, immediateRender:true}, {css:{opacity: 1}})])
		.append([TweenMax.from(jQuery('.streamlined .box:eq(5) img'), .5, {css:{opacity: 0,top:'10px'}, immediateRender:true}, {css:{opacity: 1}})]), 0, 0);
		
		
		//****************************
		
		controller.addTween('.gray-box', TweenMax.from($('.gray-box img'), .5, {css:{opacity:0,top:'10px'}}), );*/
		
	}
})