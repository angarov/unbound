<?php

add_image_size( 'blog-cover', 477, 270, true );

function theme_styles() {
	wp_enqueue_style( 'bootstrap_css', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css' );
	wp_enqueue_style( 'fontello', get_template_directory_uri() . '-child/css/fontello.css' );
	wp_enqueue_style( 'main_css', get_template_directory_uri() . '/style.css' );
}
add_action( 'wp_enqueue_scripts', 'theme_styles');

function theme_js() {
	global $wp_scripts;
	wp_enqueue_script( 'bootstrap_js', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js', array('jquery'));
	wp_enqueue_script( 'TweenMax', '//cdnjs.cloudflare.com/ajax/libs/gsap/1.20.2/TweenMax.min.js', array('jquery'));
	wp_enqueue_script( 'superscrollorama', get_template_directory_uri() . '-child/js/jquery.superscrollorama.js');
	wp_enqueue_script( 'lettering', get_template_directory_uri() . '-child/js/jquery.lettering-0.6.1.min.js');
	wp_enqueue_script( 'theme', get_template_directory_uri() . '-child/js/theme.js');
	wp_enqueue_script( 'anim', get_template_directory_uri() . '-child/js/anim.js');
	
	wp_enqueue_script( 'hs-script-loader', '//js.hs-scripts.com/1761386.js');
	
	wp_localize_script( 'theme', 'unconf', array( 'templateUrl' => get_stylesheet_directory_uri(), 'referrer' => isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '') );
}
add_action( 'wp_enqueue_scripts', 'theme_js');

function wpb_custom_company_menu() {
  register_nav_menu('company-menu',__( 'Company Menu' ));
}
add_action( 'init', 'wpb_custom_company_menu' );

function widgets_navigation_box() {
  register_sidebar(array(
    'name' => 'Navigation Box',
    'id' => 'navigation-box-sidebar',
    'before_widget' => '<div>',
    'after_widget' => '</div>',
    'before_title' => '<h1>',
    'after_title' => '</h1>',
  ));
}
add_action( 'widgets_init', 'widgets_navigation_box' );

function show_type_box($type) {
	$recent_posts = get_posts( array( 'offset' => 0, 'numberposts' => '0', 'post_status' => 'publish', 'post_type' => $type ) );
	echo '<ul class="box ' . (sizeof($recent_posts) > 4 ? "big" : "") . '">';
	foreach( $recent_posts as $recent ) {
		echo '<li class="item">';
		echo '<a href="' . get_permalink($recent->ID) . '">' . $recent->post_title . '</a>';
		echo '</li>';
	}
	echo '</ul>';
}

function custom_excerpt_length( $length ) {
	return 25;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

function usecase_list_func($atts) {
	$query = array( 'offset' => 0, 'numberposts' => isset($atts['limit']) ? $atts['limit'] : -1, 'post_status' => 'publish', 'post_type' => 'usecase' );
	if(isset($atts['id'])) {
		$query['category__in'] = $atts['id'];
	}
	
	$recent_posts = get_posts($query);
	if(count($recent_posts) == 0) {
		return;
	}
	$count = 0;
	$col = floor(12 / count($recent_posts));
	if(isset($atts['col'])) {
		$col = floor(12 / $atts['col']);
	} else if( $col < 3 ) {
		$col = 3;
	}
	
	$html = '<div class="do-for-you"><div class="row">';
	foreach($recent_posts as $post) {
		$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ) );
		$preview = get_field('preview_box', $post->ID);
		
		$html .= '<div class="col-md-' . $col . ' ' . ($count == 3 && count($recent_posts) == 5 ? 'ml16' : '') . '">';
		$html .= '<div class="box">';
		$html .= '<a href="' . get_permalink($post->ID) . '">';
		$html .= '<img src="' . $image[0] . '">';
		$html .= '<div class="a1"><p>' . $post->post_title . '</p></div><div class="a2">' . $preview . '</div>';
		$html .= '<span class="more">Read More>>></span>';
		$html .= '</a>';
		$html .= '<div class="red-bottom"></div></div>';
		$html .= '</div>';
		
		$count++;
	}
	$html .= '</div></div>';

	return $html;
}
add_shortcode( 'usecase_list', 'usecase_list_func' );

function get_post_content($post_id) {
	$content_post = get_post($post_id);
	if(!$content_post) {
		return '';
	}
	$content = $content_post->post_content;
	$content = apply_filters('the_content', $content);
	$content = str_replace(']]>', ']]&gt;', $content);
	return $content;
}
function articals_list_func($atts) {
	$id = isset($atts["page_id"]) ? $atts["page_id"] : 8571;
	return get_post_content($id);
}
add_shortcode( 'articals_list', 'articals_list_func' );

function resources_list_func($atts) {
	$args = array(
		'numberposts' => -1,
		'orderby' => 'post_date',
		'order' => 'DESC',
		'post_type' => 'resource',
		'post_status' => 'publish',
		'meta_key'		=> 'type',
		'meta_value'	=> $atts['type']
	);
	$recent_posts = wp_get_recent_posts( $args, ARRAY_A );
	
	if($atts['type'] == 'On-demand Webinar') {
		$style = 'webiners_list2';
		$col = 3;
	} else {
		$style = 'webiners_list';
		$col = (int)12 / count($recent_posts);
		if( $col < 4 ) {
			$col = 4;
		}
	}
	
	$html = '<div class="row ">';
	foreach($recent_posts as $post) {
		$featured_image_url = wp_get_attachment_url( get_post_thumbnail_id( $post["ID"] ) );
	
		$html .= '<div class="col-md-' . $col . ' ' . $style . '">';
		
		if($atts['type'] == 'On-demand Webinar') {
			$html .= '<div class="gray-box"><a href="' . get_field('resource', $post["ID"]) . '" target="_blank"><h2 style="background-color:' . get_field('background_color', $post["ID"]) . '">' . $post["post_title"] . '</h2><div class="top-cover" style="background-color:' . get_field('background_color', $post["ID"]) . '; background-image:url(' . $featured_image_url . ')"></div></a>';
		} else {
			$html .= '<div class="gray-box"><div style="background-color:' . get_field('background_color', $post["ID"]) . ';" class="top-cover"><img src="' . $featured_image_url . '"></div><h2>' . $post["post_title"] . '</h2>';
		}
		$html .= '<div class="body">' . $post["post_content"] . '</div><div class="text-center"><a href="' . get_field('resource', $post["ID"]) . '" target="_blank" class="cta">( ' . (!empty($val = get_field('action_label', $post["ID"])) ? $val : 'Watch resource') . ' )</a></div></div>'; //
		$html .= '</div>';
	}
	$html .= '</div>';
	return $html;
}
add_shortcode( 'resources_list', 'resources_list_func' );

function webiners_feature_func($atts) {
	wp_enqueue_script( 'slide', get_template_directory_uri() . '-child/js/slide.js', array('jquery'));
	
	$args = array(
		'numberposts' => -1,
		'orderby' => 'post_date',
		'order' => 'DESC',
		'post_type' => 'resource',
		'post_status' => 'publish',
		'meta_key'		=> 'type',
		'meta_value'	=> 'Upcoming Webinars'
	);
	$recent_posts = wp_get_recent_posts( $args, ARRAY_A );
	
	if(!isset($recent_posts[0])) {
		return '';
	}

	$html = '';
	foreach($recent_posts as $post) {
		$featured_image_url = wp_get_attachment_url( get_post_thumbnail_id( $post["ID"] ) );
		$html .= '<li><div class="row darker webiners_feature">
			<div class="col-md-6"><img src="' . $featured_image_url . '"></div>
			<div class="col-md-6"><h2>' . $post["post_title"] . '</h2><div>' . $post["post_content"] . '</div><a href="' . get_field('resource', $post["ID"]) . '" class="cta darker" target="_blank">( ' . (!empty($val = get_field('action_label', $post["ID"])) ? $val : 'Watch resource') . ' )</a></div>
		</div></li>';
	}
	return '<div id="slider">
				<a href="javascript:;" class="control_next"><img src="' . get_template_directory_uri() . '-child/images/br.png"></a>
				<a href="javascript:;" class="control_prev"><img src="' . get_template_directory_uri() . '-child/images/bl.png"></a>
				<ul>' . $html . '</ul>
				<div class="pagination">' . str_repeat("<a href=\"javascript:;\"></a>", count($recent_posts)) . '</div>
			</div>';
}
add_shortcode( 'webiners_feature', 'webiners_feature_func' );

function unbind_func($atts) {
	$args = array( 
		'post_type' => 'solutions',
		'post_status' => 'publish',
		'category__in' => array(isset($atts['id']) ? $atts['id'] : 58),
	);
	$recent_posts = query_posts( $args, ARRAY_A );
	
	wp_reset_query();
	$html = '<div class="row solution">';
	foreach($recent_posts as $post) {
		$html .= '<div class="col-md-4">';
		$html .= '<p><a href="' . get_permalink($post->ID) . '"><img src="' . get_template_directory_uri() . '-child/images/cl3.png" class="ll off"><img src="' . get_template_directory_uri() . '-child/images/cl3y.png" class="ll on">
				    	<img src="' . get_template_directory_uri() . '-child/images/cr3.png" class="rr off"><img src="' . get_template_directory_uri() . '-child/images/cr3y.png" class="rr on">';
		$html .= $post->post_title . '</a></p>';
		$html .= '</div>';
	}
	$html .= '</div>';
	return $html;
}
add_shortcode( 'unbind', 'unbind_func' );

function product_menu_func($atts) {
	return '<div class="container">
		<div class="tab-holder">
	        <div class="row tabs">
		        <div class="scroll">
		            <div class="col-md-3 tab selected tab-overview" data-tab="overview">Overview</div>
		            <div class="col-md-3 tab tab-features" data-tab="features">Features</div>
		            <div class="col-md-3 tab tab-deployment" data-tab="deployment">Deployment options</div>
		            <div class="col-md-3 tab tab-technical" data-tab="technical">Technical Specifications</div>
		        </div>
	        </div>
		</div>
	</div>';
}
add_shortcode( 'product_menu', 'product_menu_func' );


function team_func($atts) {
	$args = array( 
		'post_type' => 'team',
		'post_status' => 'publish',
		'category__in' => $atts['id'],
	);
	$recent_posts = query_posts( $args, ARRAY_A );
	if(count($recent_posts) == 0) {
		return '';
	}
	
	$number = (int)12 / count($recent_posts);
	
	wp_reset_query();
	$html = '<div class="row solution">';
	foreach($recent_posts as $post) {
		$featured_image_url = wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) );
	
		$html .= '<div class="col-md-' . $number .'">';
		$html .= '	<div class="team-member">';
		$html .= '	<div class="top">';
		$html .= '		<div class="photo" style="background-image:url(' . $featured_image_url . ')"></div>';
		$html .= '		<div class="info">' . $post->post_content . '</div>';
		$html .= '	</div>';
		$html .= '	<div class="bottom"><b>' . $post->post_title . '</b><br>' . get_field('title', $post->ID) . '</div>';
		$html .= '	</div>';
		$html .= '</div>';
// 		$html .= '<p><a href="' . get_permalink($post->ID) . '"><img src="' . get_template_directory_uri() . '-child/images/cl3.png" class="ll off"><img src="' . get_template_directory_uri() . '-child/images/cl3y.png" class="ll on">
// 				    	<img src="' . get_template_directory_uri() . '-child/images/cr3.png" class="rr off"><img src="' . get_template_directory_uri() . '-child/images/cr3y.png" class="rr on">';
// 		$html .= $post->post_title . '</a></p>';
	}
	$html .= '</div>';
	return $html;
}
add_shortcode( 'team', 'team_func' );

function searchfilter($query) {
 
    if ($query->is_search && !is_admin() ) {
        //$query->set('post_type',array('post'/*,'page'*/));
    }
 
return $query;
}
add_filter('pre_get_posts','searchfilter');

function the_blog_image($type) {
	
	if ( wp_is_mobile() ) {
		$image = $type == 'whats_new' ? get_field('mobile_whats_new_image') : get_field('mobile_index');
		$url = isset($image['url']) ? $image['url'] : "";
		echo '<img src="'.$url.'">';
		return;
	}

	switch($type) {
		case "whats_new":
			$image = get_field('whats_new');
		break;
		case "thumbnail_image":
			$image = get_field('most_popular_image');
		break;
		default:
			$image = get_field('index_image');
		break;
	}
	$url = isset($image['url']) ? $image['url'] : "";
	echo '<img src="'.$url.'">';
}

if ( function_exists('register_sidebar') )
  register_sidebar(array(
	  'id' => 'footerarea',
    'name' => 'Footer Area',
    'before_widget' => '<div class = "footerarea">',
    'after_widget' => '</div>',
    'before_title' => '<h3>',
    'after_title' => '</h3>',
  )
);

// add tag support to pages
function tags_support_all() {
	register_taxonomy_for_object_type('post_tag', 'page');
}

// ensure all tags are included in queries
function tags_support_query($wp_query) {
	if ($wp_query->get('tag')) $wp_query->set('post_type', 'any');
}

// tag hooks
add_action('init', 'tags_support_all');
add_action('pre_get_posts', 'tags_support_query');

// Exclude images from search results - WordPress
add_action( 'init', 'exclude_images_from_search_results' );
function exclude_images_from_search_results() {
	global $wp_post_types;
 
	$wp_post_types['attachment']->exclude_from_search = true;
}

// Register Custom Post Type
function unbound_solutions() {

	$labels = array(
		'name'                  => _x( 'Solutions', 'Post Type General Name', 'unbound' ),
		'singular_name'         => _x( 'Solution', 'Post Type Singular Name', 'unbound' ),
		'menu_name'             => __( 'Solutions', 'unbound' ),
		'name_admin_bar'        => __( 'Solutions', 'unbound' ),
		'archives'              => __( 'Item Archives', 'unbound' ),
		'attributes'            => __( 'Item Attributes', 'unbound' ),
		'parent_item_colon'     => __( 'Parent Item:', 'unbound' ),
		'all_items'             => __( 'All Items', 'unbound' ),
		'add_new_item'          => __( 'Add New Item', 'unbound' ),
		'add_new'               => __( 'Add New', 'unbound' ),
		'new_item'              => __( 'New Item', 'unbound' ),
		'edit_item'             => __( 'Edit Item', 'unbound' ),
		'update_item'           => __( 'Update Item', 'unbound' ),
		'view_item'             => __( 'View Item', 'unbound' ),
		'view_items'            => __( 'View Items', 'unbound' ),
		'search_items'          => __( 'Search Item', 'unbound' ),
		'not_found'             => __( 'Not found', 'unbound' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'unbound' ),
		'featured_image'        => __( 'Featured Image', 'unbound' ),
		'set_featured_image'    => __( 'Set featured image', 'unbound' ),
		'remove_featured_image' => __( 'Remove featured image', 'unbound' ),
		'use_featured_image'    => __( 'Use as featured image', 'unbound' ),
		'insert_into_item'      => __( 'Insert into item', 'unbound' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'unbound' ),
		'items_list'            => __( 'Items list', 'unbound' ),
		'items_list_navigation' => __( 'Items list navigation', 'unbound' ),
		'filter_items_list'     => __( 'Filter items list', 'unbound' ),
	);
	$args = array(
		'label'                 => __( 'Solution', 'unbound' ),
		'description'           => __( 'Post Type Description', 'unbound' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-sos',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'show_in_rest'          => true,
	);
	register_post_type( 'solutions', $args );

}
add_action( 'init', 'unbound_solutions', 0 );

function unbound_usecase() {

	$labels = array(
		'name'                  => _x( 'Use Case', 'Post Type General Name', 'unbound' ),
		'singular_name'         => _x( 'Use Case', 'Post Type Singular Name', 'unbound' ),
		'menu_name'             => __( 'Use Cases', 'unbound' ),
		'name_admin_bar'        => __( 'Use Cases', 'unbound' ),
		'archives'              => __( 'Item Archives', 'unbound' ),
		'attributes'            => __( 'Item Attributes', 'unbound' ),
		'parent_item_colon'     => __( 'Parent Item:', 'unbound' ),
		'all_items'             => __( 'All Items', 'unbound' ),
		'add_new_item'          => __( 'Add New Item', 'unbound' ),
		'add_new'               => __( 'Add New', 'unbound' ),
		'new_item'              => __( 'New Item', 'unbound' ),
		'edit_item'             => __( 'Edit Item', 'unbound' ),
		'update_item'           => __( 'Update Item', 'unbound' ),
		'view_item'             => __( 'View Item', 'unbound' ),
		'view_items'            => __( 'View Items', 'unbound' ),
		'search_items'          => __( 'Search Item', 'unbound' ),
		'not_found'             => __( 'Not found', 'unbound' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'unbound' ),
		'featured_image'        => __( 'Featured Image', 'unbound' ),
		'set_featured_image'    => __( 'Set featured image', 'unbound' ),
		'remove_featured_image' => __( 'Remove featured image', 'unbound' ),
		'use_featured_image'    => __( 'Use as featured image', 'unbound' ),
		'insert_into_item'      => __( 'Insert into item', 'unbound' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'unbound' ),
		'items_list'            => __( 'Items list', 'unbound' ),
		'items_list_navigation' => __( 'Items list navigation', 'unbound' ),
		'filter_items_list'     => __( 'Filter items list', 'unbound' ),
	);
	$args = array(
		'label'                 => __( 'Use Case', 'unbound' ),
		'description'           => __( 'Post Type Description', 'unbound' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-welcome-learn-more',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'show_in_rest'          => true,
	);
	register_post_type( 'usecase', $args );

}
add_action( 'init', 'unbound_usecase', 0 );

function unbound_product() {

	$labels = array(
		'name'                  => _x( 'Product', 'Post Type General Name', 'unbound' ),
		'singular_name'         => _x( 'Product', 'Post Type Singular Name', 'unbound' ),
		'menu_name'             => __( 'Products', 'unbound' ),
		'name_admin_bar'        => __( 'Product', 'unbound' ),
		'archives'              => __( 'Item Archives', 'unbound' ),
		'attributes'            => __( 'Item Attributes', 'unbound' ),
		'parent_item_colon'     => __( 'Parent Item:', 'unbound' ),
		'all_items'             => __( 'All Items', 'unbound' ),
		'add_new_item'          => __( 'Add New Item', 'unbound' ),
		'add_new'               => __( 'Add New', 'unbound' ),
		'new_item'              => __( 'New Item', 'unbound' ),
		'edit_item'             => __( 'Edit Item', 'unbound' ),
		'update_item'           => __( 'Update Item', 'unbound' ),
		'view_item'             => __( 'View Item', 'unbound' ),
		'view_items'            => __( 'View Items', 'unbound' ),
		'search_items'          => __( 'Search Item', 'unbound' ),
		'not_found'             => __( 'Not found', 'unbound' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'unbound' ),
		'featured_image'        => __( 'Featured Image', 'unbound' ),
		'set_featured_image'    => __( 'Set featured image', 'unbound' ),
		'remove_featured_image' => __( 'Remove featured image', 'unbound' ),
		'use_featured_image'    => __( 'Use as featured image', 'unbound' ),
		'insert_into_item'      => __( 'Insert into item', 'unbound' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'unbound' ),
		'items_list'            => __( 'Items list', 'unbound' ),
		'items_list_navigation' => __( 'Items list navigation', 'unbound' ),
		'filter_items_list'     => __( 'Filter items list', 'unbound' ),
	);
	$args = array(
		'label'                 => __( 'Project', 'unbound' ),
		'description'           => __( 'Post Type Description', 'unbound' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'page-attributes'),
// 		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => true,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-flag',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'show_in_rest'          => true,
	);
	register_post_type( 'product', $args );

}
add_action( 'init', 'unbound_product', 0 );



function unbound_events() {

	$labels = array(
		'name'                  => _x( 'Event', 'Post Type General Name', 'unbound' ),
		'singular_name'         => _x( 'Event', 'Post Type Singular Name', 'unbound' ),
		'menu_name'             => __( 'Events', 'unbound' ),
		'name_admin_bar'        => __( 'Events', 'unbound' ),
		'archives'              => __( 'Item Archives', 'unbound' ),
		'attributes'            => __( 'Item Attributes', 'unbound' ),
		'parent_item_colon'     => __( 'Parent Item:', 'unbound' ),
		'all_items'             => __( 'All Items', 'unbound' ),
		'add_new_item'          => __( 'Add New Item', 'unbound' ),
		'add_new'               => __( 'Add New', 'unbound' ),
		'new_item'              => __( 'New Item', 'unbound' ),
		'edit_item'             => __( 'Edit Item', 'unbound' ),
		'update_item'           => __( 'Update Item', 'unbound' ),
		'view_item'             => __( 'View Item', 'unbound' ),
		'view_items'            => __( 'View Items', 'unbound' ),
		'search_items'          => __( 'Search Item', 'unbound' ),
		'not_found'             => __( 'Not found', 'unbound' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'unbound' ),
		'featured_image'        => __( 'Featured Image', 'unbound' ),
		'set_featured_image'    => __( 'Set featured image', 'unbound' ),
		'remove_featured_image' => __( 'Remove featured image', 'unbound' ),
		'use_featured_image'    => __( 'Use as featured image', 'unbound' ),
		'insert_into_item'      => __( 'Insert into item', 'unbound' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'unbound' ),
		'items_list'            => __( 'Items list', 'unbound' ),
		'items_list_navigation' => __( 'Items list navigation', 'unbound' ),
		'filter_items_list'     => __( 'Filter items list', 'unbound' ),
	);
	$args = array(
		'label'                 => __( 'Event', 'unbound' ),
		'description'           => __( 'Post Type Description', 'unbound' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-calendar-alt',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'show_in_rest'          => true,
	);
	register_post_type( 'event', $args );

}
add_action( 'init', 'unbound_events', 0 );


function unbound_news() {

	$labels = array(
		'name'                  => _x( 'News', 'Post Type General Name', 'unbound' ),
		'singular_name'         => _x( 'News', 'Post Type Singular Name', 'unbound' ),
		'menu_name'             => __( 'News', 'unbound' ),
		'name_admin_bar'        => __( 'News', 'unbound' ),
		'archives'              => __( 'Item Archives', 'unbound' ),
		'attributes'            => __( 'Item Attributes', 'unbound' ),
		'parent_item_colon'     => __( 'Parent Item:', 'unbound' ),
		'all_items'             => __( 'All Items', 'unbound' ),
		'add_new_item'          => __( 'Add New Item', 'unbound' ),
		'add_new'               => __( 'Add New', 'unbound' ),
		'new_item'              => __( 'New Item', 'unbound' ),
		'edit_item'             => __( 'Edit Item', 'unbound' ),
		'update_item'           => __( 'Update Item', 'unbound' ),
		'view_item'             => __( 'View Item', 'unbound' ),
		'view_items'            => __( 'View Items', 'unbound' ),
		'search_items'          => __( 'Search Item', 'unbound' ),
		'not_found'             => __( 'Not found', 'unbound' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'unbound' ),
		'featured_image'        => __( 'Featured Image', 'unbound' ),
		'set_featured_image'    => __( 'Set featured image', 'unbound' ),
		'remove_featured_image' => __( 'Remove featured image', 'unbound' ),
		'use_featured_image'    => __( 'Use as featured image', 'unbound' ),
		'insert_into_item'      => __( 'Insert into item', 'unbound' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'unbound' ),
		'items_list'            => __( 'Items list', 'unbound' ),
		'items_list_navigation' => __( 'Items list navigation', 'unbound' ),
		'filter_items_list'     => __( 'Filter items list', 'unbound' ),
	);
	$args = array(
		'label'                 => __( 'News', 'unbound' ),
		'description'           => __( 'Post Type Description', 'unbound' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-megaphone',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'show_in_rest'          => true,
	);
	register_post_type( 'news', $args );

}
add_action( 'init', 'unbound_news', 0 );


function unbound_resource() {

	$labels = array(
		'name'                  => _x( 'Resources', 'Post Type General Name', 'unbound' ),
		'singular_name'         => _x( 'Resource', 'Post Type Singular Name', 'unbound' ),
		'menu_name'             => __( 'Resources', 'unbound' ),
		'name_admin_bar'        => __( 'Resources', 'unbound' ),
		'archives'              => __( 'Item Archives', 'unbound' ),
		'attributes'            => __( 'Item Attributes', 'unbound' ),
		'parent_item_colon'     => __( 'Parent Item:', 'unbound' ),
		'all_items'             => __( 'All Items', 'unbound' ),
		'add_new_item'          => __( 'Add New Item', 'unbound' ),
		'add_new'               => __( 'Add New', 'unbound' ),
		'new_item'              => __( 'New Item', 'unbound' ),
		'edit_item'             => __( 'Edit Item', 'unbound' ),
		'update_item'           => __( 'Update Item', 'unbound' ),
		'view_item'             => __( 'View Item', 'unbound' ),
		'view_items'            => __( 'View Items', 'unbound' ),
		'search_items'          => __( 'Search Item', 'unbound' ),
		'not_found'             => __( 'Not found', 'unbound' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'unbound' ),
		'featured_image'        => __( 'Featured Image', 'unbound' ),
		'set_featured_image'    => __( 'Set featured image', 'unbound' ),
		'remove_featured_image' => __( 'Remove featured image', 'unbound' ),
		'use_featured_image'    => __( 'Use as featured image', 'unbound' ),
		'insert_into_item'      => __( 'Insert into item', 'unbound' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'unbound' ),
		'items_list'            => __( 'Items list', 'unbound' ),
		'items_list_navigation' => __( 'Items list navigation', 'unbound' ),
		'filter_items_list'     => __( 'Filter items list', 'unbound' ),
	);
	$args = array(
		'label'                 => __( 'Resources', 'unbound' ),
		'description'           => __( 'Post Type Description', 'unbound' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-video-alt2',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'show_in_rest'          => true,
	);
	register_post_type( 'resource', $args );

}
add_action( 'init', 'unbound_resource', 0 );


function unbound_team() {

	$labels = array(
		'name'                  => _x( 'Team', 'Post Type General Name', 'unbound' ),
		'singular_name'         => _x( 'Team', 'Post Type Singular Name', 'unbound' ),
		'menu_name'             => __( 'Team', 'unbound' ),
		'name_admin_bar'        => __( 'Team', 'unbound' ),
		'archives'              => __( 'Item Archives', 'unbound' ),
		'attributes'            => __( 'Item Attributes', 'unbound' ),
		'parent_item_colon'     => __( 'Parent Item:', 'unbound' ),
		'all_items'             => __( 'All Items', 'unbound' ),
		'add_new_item'          => __( 'Add New Item', 'unbound' ),
		'add_new'               => __( 'Add New', 'unbound' ),
		'new_item'              => __( 'New Item', 'unbound' ),
		'edit_item'             => __( 'Edit Item', 'unbound' ),
		'update_item'           => __( 'Update Item', 'unbound' ),
		'view_item'             => __( 'View Item', 'unbound' ),
		'view_items'            => __( 'View Items', 'unbound' ),
		'search_items'          => __( 'Search Item', 'unbound' ),
		'not_found'             => __( 'Not found', 'unbound' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'unbound' ),
		'featured_image'        => __( 'Featured Image', 'unbound' ),
		'set_featured_image'    => __( 'Set featured image', 'unbound' ),
		'remove_featured_image' => __( 'Remove featured image', 'unbound' ),
		'use_featured_image'    => __( 'Use as featured image', 'unbound' ),
		'insert_into_item'      => __( 'Insert into item', 'unbound' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'unbound' ),
		'items_list'            => __( 'Items list', 'unbound' ),
		'items_list_navigation' => __( 'Items list navigation', 'unbound' ),
		'filter_items_list'     => __( 'Filter items list', 'unbound' ),
	);
	$args = array(
		'label'                 => __( 'Resources', 'unbound' ),
		'description'           => __( 'Post Type Description', 'unbound' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-groups',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
		'show_in_rest'          => true,
	);
	register_post_type( 'team', $args );

}
add_action( 'init', 'unbound_team', 0 );


