<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Unbound
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> >
	<div class="row">
	    <div class="col-md-2">
			<div class="featured"><?php the_post_thumbnail(); ?></div>
	    </div>
	    <div class="col-md-8">
			<div class="entry-content">
				<?php
					the_title( '<h2 class="entry-title">', '</h2>' );
					the_content();
				?>
				<a href="<?php the_field('news_link'); ?>" target="_blank">Read More >></a>
			</div>
	    </div>
	    <div class="col-md-2">
			<div class="entry-meta">
				<?php the_field('news_date'); ?>
			</div>
	    </div>
	</div>
</article><!-- #post-<?php the_ID(); ?> -->
