<?php
/**
* Template Name: Free Trail
*
*/
get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<div class="container">
				<header class="title">
					<?php the_title(); ?>
				</header>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();