<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Unbound
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('full'); ?>>
	
	<header class="entry-header">
		<div class="container">
            <div class="row">
                <div class="col-md-15 whatsnew">
					<div class="image-cover"><?php the_blog_image('whats_new'); ?></div>
					<div class="top-blog">
		                <b>What’s New</b>
						<?php the_title( '<h2 class="entry-title">', '</h2>' ); ?>
						<p><?php echo the_excerpt(); ?></p>
						<a href="<?php the_permalink(); ?>;" class="cta">( Read this Post )</a>
					</div>
                </div>
            </div>
		</div>
	</header>
		
</article><!-- #post-<?php the_ID(); ?> -->
