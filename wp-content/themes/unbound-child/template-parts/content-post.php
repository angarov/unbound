<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Unbound
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="author">
		<?php echo get_avatar(get_the_author_meta('ID')); ?>
		<h2><?php the_author(); ?></h2>
		<p><?php the_author_meta('description'); ?></p>
		<b><?php unbound_posted_on(); ?></b>
		<div class="share"><div class="fb-like" data-href="<?php echo esc_url( get_permalink() ); ?>" data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="false"></div></div>
	</div>
	<h1><?php the_title( '<h1 class="entry-title">', '</h1>' ); ?></h1>
	<div class="cover">
	<?php


		if( ($video = get_post_meta(get_the_ID(), '_blog_video')) != false ) {
			preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $video[0], $match);
			if( isset($match[1]) ) {
				$youtube_id = $match[1];
				echo '<iframe width="100%" height="550" src="https://www.youtube.com/embed/' . $youtube_id . '" frameborder="0" allowfullscreen></iframe>';
			}
		} /*else {
			the_blog_image('index_image');
		}*/
	?>
	</div>

	<div class="entry-content">
		<?php
			the_content();

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'unbound' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->
	
</article><!-- #post-<?php the_ID(); ?> -->
