<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Unbound
 */
 
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="featured">
	<?php
		the_post_thumbnail();
	?>
	</div>
	<div class="entry-body">
		<?php
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		?>
		<div class="entry-content">
			<?php
				the_excerpt( sprintf(
					wp_kses(
						/* translators: %s: Name of current post. Only visible to screen readers */
						__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'unbound' ),
						array(
							'span' => array(
								'class' => array(),
							),
						)
					),
					get_the_title()
				) );
	
				wp_link_pages( array(
					'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'unbound' ),
					'after'  => '</div>',
				) );
			?>
		</div>
		<a href="<?php the_permalink() ?>"><?php the_permalink() ?></a>
	</div><!-- .entry-header -->

	<!-- .entry-content -->
</article><!-- #post-<?php the_ID(); ?> -->
