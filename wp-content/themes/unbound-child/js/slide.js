jQuery(document).ready(function (jQuery) {
	
	var slideCount = 0;
	function slider(index) {
		if(index < 0) index = jQuery('#slider ul li').get().length - 1;
		if(index >= jQuery('#slider ul li').get().length ) index = 0;
		slideCount = index;
		
        jQuery('#slider ul li').removeClass('active');
        jQuery('#slider ul li:eq('+slideCount+')').addClass('active');
        
        jQuery('#slider .pagination a').removeClass('active');
        jQuery('#slider .pagination a:eq('+slideCount+')').addClass('active');
        
        
	}
    
	jQuery('a.control_prev').click(function () {
        slider(slideCount + 1)
    });

    jQuery('a.control_next').click(function () {
        slider(slideCount - 1)
    });
    
    jQuery('#slider .pagination a').click(function () {
        slider(jQuery(this).index());
    });

	slider(0);
}); 