<?php
/**
* Template Name: Crypto-of-Things
* Template Post Type: product
*/

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
		<?php
		while ( have_posts() ) : the_post();

?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
	<header class="entry-header">
		<div class="container">
            <div class="row">
                <div class="col-md-6 pull-right">
					<h1>Unbound<br><?php echo get_the_title(); ?></h1>
                </div>
                <div class="col-md-6 pull-left">
					<img src="<?php echo get_template_directory_uri(); ?>-child/images/comp.jpg" class="header1">
                </div>
                <div class="col-md-6 pull-right">
					<h3>Secure keys and secretson any device</h3>
					<p>The first software-only solution to protect keys at hardware-level security over any platform, from mobile to desktop to IoT </p>
					<a href="<?php the_permalink(71); ?>" class="cta">( Contact us )</a>
                </div>
            </div>
		</div>
	</header>
	
	<div class="container">
		<div class="tab-holder">
	        <div class="row tabs">
		        <div class="scroll">
		            <div class="col-md-3 tab selected" data-tab="overview">Overview</div>
		            <div class="col-md-3 tab" data-tab="features">Features</div>
		            <div class="col-md-3 tab" data-tab="deployment">Deployment options</div>
		            <div class="col-md-3 tab" data-tab="technical">Technical Specifications</div>
		        </div>
	        </div>
		</div>
	</div>
	
	<div class="tabs-content" id="tab-overview">

		<div class="all-in-one">
		    <div class="container">
		        <h2>A Virtual Secure Enclave For Any App On Any Device</h2>
		        <p>Drawing it's strength for Unbound's vHSM technology, Unbound Crypto of Things (CoT) lets developers simplify and strengthen the identity and authentication crypto layers of their mobile apps in hetergenous environments. CoT unbinds the crypto layer from the hardware devices, so you don't have to develop for every device make or model. CoT let's you virtually embed a secure enclave to protect keys and certificates on any and every device.</p>
		        
		        <div class="boxes">
			        <div class="row">
				    	<div class="plus"><img src="<?php echo get_template_directory_uri(); ?>-child/images/plus.png"></div>
				    	<div class="col-md-6">
					    	<div class="box">
								<h3>Key management </h3>
								<p>Full key lifecycle management</p>
								<p>Manage any key - anywhere: on-premiseand private/public cloud</p>
								<p>Central management of keys and certificates</p>
							</div>
				    	</div>
				    	<div class="col-md-6">
					    	<div class="box">
								<h3>key protection</h3>
								<p>Hardware-Level Security</p>
								<p>Supports all standard crypto APIs</p>
								<p>Supports all standard keys and algorithms</p>
							</div>
				    	</div>
			        </div>
		        </div>
		    </div>
		</div>
		
	    <div class="stretching darker">
	    	<div class="container">
		    	<div class="row">
			    	<div class="col-md-2">
				    	<img src="<?php echo get_template_directory_uri(); ?>-child/images/lock21.png" class="ll">
			    	</div>
			    	<div class="col-md-8">
			            <h2>Eliminating the Single Point of Failure</h2>
			            <p>Unbound CoT eliminates this single point of failure by ensuring that your most sensitive keys never exist in the clear at any point in their lifecycle. With Unbound, key material is never whole. Rather, each key exists as two random key shares, separated completely: between two separate clouds, or between a hybrid topology.</p>
			
						<p>All operations are carried out without ever uniting the key shares. By eliminating the single point of failure, Unbound CoT can stretch the secure boundary far beyond the traditional physical casing.</p>
					    <a href="<?php the_permalink(17); ?>" class="cta darker">( Learn How )</a>
			    	</div>
			    	<div class="col-md-2">
				    	<img src="<?php echo get_template_directory_uri(); ?>-child/images/lock22.png" class="rr">
			    	</div>
		    	</div>
	    	</div>
	    </div>
	    
	    <div class="do-for-you for-project">
	    	<div class="container max1000">
	            <h2>What's Made Possible with Unbound Crypto-of-Things?</h2>
				<div class="row">
				<?php
					$args = array( 
						'post_type' => 'usecase',
						'post_status' => 'publish',
						'category__in' => array(59),
					);
					$recent_posts = query_posts( $args, ARRAY_A );
					wp_reset_query();
					
					foreach( $recent_posts as $recent ):
						$image = wp_get_attachment_image_src( get_post_thumbnail_id( $recent->ID ) );
				?>
					<div class="col-md-4">
						<div class="box">
							<a href="<?php echo get_permalink($recent->ID); ?>">
								<img src="<?php echo $image[0]; ?>">
								<div class="a1"><p><?php echo $recent->post_title; ?></p></div><div class="a2"><?php echo get_field('preview_box', $recent->ID); ?></div>
								<span class="more">Read More>>></span>
							</a>
							<div class="red-bottom"></div>
						</div>
					</div>
				<?php endforeach; ?>
				</div>
	        </div>
	    </div>
		
	    <div class="what-to">
	    	<div class="container">
		    	<h2>What Would You Like to Unbind?</h2>
		    	<?php echo do_shortcode("[unbind cat=58]" ) ?>
	    	</div>
	    </div>
	        
		<div class="darker-row darker">
		    <div class="container">
		        <h2>Protect and Manage Keys On Any Device</h2>
				<a href="<?php the_permalink(71); ?>" class="cta darker">( Contact us )</a>
		    </div>
		</div>

		<?php echo do_shortcode("[articals_list limit=4]"); ?>
	</div>
	
	<div class="tabs-content" id="tab-features">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<div class="image"><img src="<?php echo get_template_directory_uri(); ?>-child/images/f10.png"></div>
				</div>
				<div class="col-md-6">
					<h2>Central Management and Real Time Tamper Proof Audit Trail</h2>
					<p>Unbound CoT requires communication between the endpoint and the CoT server for performing any crypto operation. Thus, the CoT server includes real time tamper-proof audit log of any crypto operation performed on the endpoint, allowing detection of crypto key usage anomalies in real time.</p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 pull-right">
					<div class="image"><img src="<?php echo get_template_directory_uri(); ?>-child/images/f11.png"></div>
				</div>
				<div class="col-md-6">
					<h2>Instant Revocation </h2>
					<p>In case an endpoint is suspected as compromised, Unbound vSE allows ultimate control by ensuring instant revocation of any crypto key that is secured with vSE. Deletion of the relevant key share on the vSE server immediately renders the key useless, ensuring that any assets protected by this key are safe. </p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="image"><img src="<?php echo get_template_directory_uri(); ?>-child/images/f12.png"></div>
				</div>
				<div class="col-md-6">
					<h2>Brute-force Proof Authentication Factors</h2>
					<p>Unbound CoT provides various authentication factors that can be used to authorize any usage of the cryptographic key. The authentication takes place using MPC algorithm between the endpoint and the vSE server, thus preventing brute force attacks on the endpoint side. </p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 pull-right">
					<div class="image"><img src="<?php echo get_template_directory_uri(); ?>-child/images/f13.png"></div>
				</div>
				<div class="col-md-6">
					<h2>Seamless Integration</h2>
					<p>Unbound CoT is very simple to integrate within your applications, with a simple, intuitive and easy to use SDK. The software is very lightweight and integrates with virtually any device, including laptops, mobile, wearables and other IoT devices.</p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="image"><img src="<?php echo get_template_directory_uri(); ?>-child/images/f14.png"></div>
				</div>
				<div class="col-md-6">
					<h2>Future-ready and Agile Cryptography </h2>
					<p>Dyadic CoT can be deployed on virtually any endpoint, from IoT and mobile to laptops and even application servers and containers in the cloud / data center. Expanding the secure boundary of endpoint devices to include a central server allows unparalleled level of real-time control and audit.</p>
				</div>
			</div>
		</div>
	</div>
	
	<div class="tabs-content" id="tab-deployment">
		<div class="container">
			<h2>Architecture: Non-continuous Secure Boundary</h2>
			<p>Each Unbound CoT system is comprised of a central server (CoT server) that is installed and managed by the customer. Various endpoint devices that run CoT software (CoT library) connect to the CoT server, creating a series of pairs - where each pair consists of a single endpoint device and the CoT server. Each of the pair nodes hold one share of a key. Together, CoT software on the device and the CoT server form the secure boundary of Unbound CoT.</p>
			
			<img src="<?php echo get_template_directory_uri(); ?>-child/images/map4.png" class="map2">
			
			<p>Applications on the device use the CoT library API for consuming cryptographic service for the keys that are managed within the library, effectively creating a virtual secure enclave on the device. All connections between CoT devices to the CoT server are protected using server authentication (TLS). 
Key shares are constantly refreshed, so in order to maliciously obtain key material an attacker must compromise both the device and the CoT server simultaneously.</p>
			
		</div>
		   
		<div class="gray-grid secured-by">
		    <div class="container">
		        <h2>Secured By a Root of Trust</h2>
		        <div class="large-box">
			        <div class="row">
						<div class="col-md-7">
							<img src="<?php echo get_template_directory_uri(); ?>-child/images/map6.png" class="map2">
						</div>
						<div class="col-md-5">
							<h3>Root of Trust for Any Endpoint Device </h3>
							<p>
								Unbound CoT completely abstracts the underlying hardware, effectively enhancing any endpoint device with a virtual root of trust that has a unified, single API used among all devices and platforms supported. In addition, the CoT utilizes secure hardware if such exist on the device, to provide even higher level of security.
							</p>
						</div>
			        </div>
		        </div>
		        <div class="large-box">
			        <div class="row">
						<div class="col-md-5">
							<h3>Root of Trust for Applications and Containers in the Cloud and Data Center </h3>
							<p>Unbound CoT effectively creats a virtual root of trust for any application, server or container in the cloud or data center. In addition, the CoT utilizes secure hardware such as a TPM or Intel Software Guard Extensions (SGX) if such exist on the endpoint, to provide even higher level of security.							</p>
						</div>
						<div class="col-md-7">
							<img src="<?php echo get_template_directory_uri(); ?>-child/images/map5.png" class="map2">
						</div>
			        </div>
		        </div>
		    </div>
		</div>
		
		<div class="onprem darker darker-row">
		    <div class="container">
				<div class="row">
			    	<div class="col-md-3">
				    	<img src="<?php echo get_template_directory_uri(); ?>-child/images/lock3.png" class="ll">
			    	</div>
			    	<div class="col-md-6">
			            <h2>Security That Goes Beyond</h2>
						<p>The CoT limitless Secure Boundary adds a newly created dimension to security architectures. The inherent separation between and endpoint device and a remote server stretches the secure boundary far beyond the traditional physical casing of the device, ensuring that keys on endpoint devices remain secure at all times, even in the presence of malware or malicious actor fully controlling the endpoint device.</p>
		        	</div>
			    	<div class="col-md-3">
				    	<img src="<?php echo get_template_directory_uri(); ?>-child/images/lock3.png" class="rr">
			    	</div>
		    	</div>
		    </div>
		</div>
		   
		<div class="gray-grid secured-by-end">
		    <div class="container">
		        <h2>Protect and Manage Keys On Any Device</h2>
		        <a href="<?php the_permalink(71); ?>" class="cta">( Let's talk )</a>
		    </div>
		</div>

		<?php echo do_shortcode("[articals_list limit=4]"); ?>
		
	</div>
	
	<div class="tabs-content" id="tab-technical">
		<div class="container">
			<div class="row">
				<div class="col-md-2"><img src="<?php echo get_template_directory_uri(); ?>-child/images/t8.png" class="map2"></div>
				<div class="col-md-10">
					<h2>Operating Systems and Platform</h2>
					<table>
						<tr>
							<th rowspan="4" class="gray">Component</th>
							<th class="gray">Device Type</th>
							<th class="gray">Supported Operating Systems</th>
						</tr>
						<tr>
							<td>Mobile (smartphones, tablets, wearables) </td>
							<td>Android, iOS</td>
						</tr>
						<tr>
							<td>Desktop/laptop</td>
							<td>Windows , Mac, Linux</td>
						</tr>
						<tr>
							<td>Virtual/physical server, container </td>
							<td>Linux, Windows</td>
						</tr>
						<tr>
							<td class="gray">CoT Server</td>
							<td>Virtual/physical server</td>
							<td>Linux, Windows</td>
						</tr>
						
					</table>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h2><img src="<?php echo get_template_directory_uri(); ?>-child/images/t1.png" class="map2">API Support</h2>
					<ul>
						<li>Mobile: Simple and easy to use SDK</li>
						<li>Desktop/laptop/server: PKCS#11, Java (JCE) Microsoft CNG, OpenSSL</li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h2><img src="<?php echo get_template_directory_uri(); ?>-child/images/t2.png" class="map2">Cryptography</h2>
					<ul>
						<li>Full Suite B support</li>
						<li>Asymmetric: RSA (2048, 3072, 4096), Elliptic Curve Cryptography with P256 | P384 | P521 curves</li>
						<li>Symmetric: AES (128, 256</li>
						<li>Hash/HMAC: SHA-256, SHA-384</li>
						<li>Proprietary algorithms: Secure password verification using PIN/Native biometrics, Post-Quantum Crypto (PQC) , Bitcoin and blockchain, generic secrets </li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h2><img src="<?php echo get_template_directory_uri(); ?>-child/images/t3.png" class="map2">Endpoint Additional Authentication</h2>
					<ul>
						<li>Device-native fingerprint/Face Recognition </li>
						<li>Device-native fingerprint/Face Recognition </li>
						<li>SAML</li>
						<li>PIN, password</li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h2><img src="<?php echo get_template_directory_uri(); ?>-child/images/t9.png" class="map2">Leverage Secure Element (SE) & Trusted Execution Environment (TEE)</h2>
					<ul>
						<li>Mobile: iOS secure element, Android TEE</li>
						<li>Desktop/laptop/server: TPM, TXT, SGX </li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h2><img src="<?php echo get_template_directory_uri(); ?>-child/images/t4.png" class="map2">High Availability</h2>
					<ul>
						<li>Active/Active and Active/Passive modes (with external load balancer)</li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h2><img src="<?php echo get_template_directory_uri(); ?>-child/images/t5.png" class="map2">Management and administration</h2>
					<ul>
						<li>Command Line Interface (CLI)</li>
						<li>Management REST API </li>
						<li>Full multi-tenancy support with cryptographically isolated domains</li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h2><img src="<?php echo get_template_directory_uri(); ?>-child/images/t6.png" class="map2">Performance Specifications</h2>
					<ul>	
						<li>Cryptographically isolated domains: up to 10,000</li>
						<li>Maximum total endpoints for all tenants commutatively: up to 250,000,000</li>
						<li>Keys: bound by disk space only</li>
						<li>Capacity in transactions per second (TPS)  for sample configurations:</li>
					</ul>
					<table class="with-pp">
						<tr>
							<th class="gray"></th>
							<th class="gray pp-start">Basic Unit</th>
							<th class="gray">Sample 'S' Cluster</th>
							<th class="gray">Sample 'M' Cluster</th>
							<th class="gray">Sample 'L' Cluster</th>
						</tr>
						<tr>
							<td class="gray remove-bt"></td>
							<td class="pp">1 pair of servers,<br>1 core per server</td>
							<td>1 pair of servers,<br>2 cores per server</td>
							<td>2 pairs of servers,<br>4 cores per server</td>
							<td>4 pairs of servers,<br>8 cores per server</td>
						</tr>
						<tr>
							<td class="gray">RSA-2048</td>
							<td class="pp">100</td>
							<td>200</td>
							<td>800</td>
							<td>3200</td>
						</tr>
						<tr>
							<td class="gray">ECIES P256 </td>
							<td class="pp">100</td>
							<td>200</td>
							<td>800</td>
							<td>3200</td>
						</tr>
						<tr>
							<td class="gray">AES-GCM 128 single block</td>
							<td class="pp-end">200 <img src="<?php echo get_template_directory_uri(); ?>-child/images/arrow1.png" class="map2"></td>
							<td>400</td>
							<td>1600</td>
							<td>6400</td>
						</tr>
						
					</table>
					<p class="cpu">
						Capacity is derived from the number of CPU cores in the EKM cluster. Scaling the Basic EKM Unit is done by scaling up or scaling out, and is fully linear, as illustrated in the sample clusters above
					</p>
				</div>
			</div>
		</div>
		<div class="darker p123d">
		    <div class="container">
		        <h2>Protect and Manage Keys On Any Device</h2>
		        <a href="<?php the_permalink(71); ?>" class="cta darker">( Let's talk )</a>
		    </div>
		</div>

		<?php echo do_shortcode("[articals_list limit=4]"); ?>
	</div>
	
</article><!-- #post-<?php the_ID(); ?> -->
<?php

		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
