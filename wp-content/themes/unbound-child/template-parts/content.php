<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Unbound
 */
 
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="cover">
	<?php
		if( ($video = get_post_meta(get_the_ID(), '_blog_video')) != false ) {
			preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $video[0], $match);
			if( isset($match[1]) ) {
				$youtube_id = $match[1];
				echo '<iframe width="100%" height="226" src="https://www.youtube.com/embed/' . $youtube_id . '" frameborder="0" allowfullscreen></iframe>';
			} else {
				the_blog_image('index_image');
			}
		} else {
			the_blog_image('index_image');
		}
		/*$featured_image_url = wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() ) );


		if  ( ! empty( $featured_image_url ) ) {
			the_post_thumbnail('blog-cover'); //'blog-cover'
		} else if( ($video = get_post_meta(get_the_ID(), '_blog_video')) != false ) {
			preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $video[0], $match);
			$youtube_id = $match[1];
			echo '<iframe width="100%" height="226" src="https://www.youtube.com/embed/' . $youtube_id . '" frameborder="0" allowfullscreen></iframe>';
		} else {
			echo wp_get_attachment_image(7693, 'full');
		}*/
	?>
	</div>

	<div class="author">
		<?php echo get_avatar(get_the_author_meta('ID')); ?>
		<?php the_author(); ?>
	</div>
	<header class="entry-header">
		<?php
		if ( is_singular() ) :
			the_title( '<h1 class="entry-title">', '</h1>' );
		else :
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif;

		if ( 'post' === get_post_type() ) : ?>
		<div class="entry-meta">
			<?php unbound_posted_on(); ?>
		</div><!-- .entry-meta -->
		<?php
		endif; ?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php
			the_excerpt( sprintf(
				wp_kses(
					/* translators: %s: Name of current post. Only visible to screen readers */
					__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'unbound' ),
					array(
						'span' => array(
							'class' => array(),
						),
					)
				),
				get_the_title()
			) );

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'unbound' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<a href="<?php echo esc_url( get_permalink() ); ?>" class="cta" rel="bookmark">( Read this Post )</a>
		<div class="share"><div class="fb-like" data-href="<?php echo esc_url( get_permalink() ); ?>" data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="false"></div></div>
	</footer><!-- .entry-footer -->
</article><!-- #post-<?php the_ID(); ?> -->
