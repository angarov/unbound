<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Unbound
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
	<header class="entry-header">
		<div class="container">
            <div class="row">
                <div class="col-md-3">
					<img src="<?php echo get_template_directory_uri(); ?>-child/images/header4.png" class="header1">
                </div>
                <div class="col-md-6">
					<h1><?php echo get_the_title(); ?></h1>
					<p>Break the boundaries of key management between on premise,public and private clouds with Unbound Crypto Orchestration</p>
					<a href="javascript:;" class="cta">( Let’s Talk )</a>
                </div>
                <div class="col-md-3">
					<img src="<?php echo get_template_directory_uri(); ?>-child/images/header5.png" class="header1">
                </div>
            </div>
		</div>
	</header>
	
	<div class="silos-means">
	    <div class="container">
	        <h2>Managing Keys in Silos Means More Complexity and More Risk</h2>
	        <p>Because key management is mostly confined to physical infrastructure,businesses face complexity and security risk.
In the case of a hybrid environment, multiple siloed key management systems must co-exist both on the cloud and on premise. In a geographically dispersed environment, key management systems are distributed on premises across the globe.</p>
	    </div>
	</div>
	
    <div class="centralized darker">
	    <div class="container">
	        <h2>Centralized Key Management Across All Sites</h2>
	        <p class="subtitle">Unbound uproots the silos created by hybrid and geo-distributed environments with the complete abstraction of key management - eliminating any dependency on the underlying hardware and physical infrastructure. It’s now possible to manage keys across all sites from one centralized infrastructure.</p>
	        
	        <div class="row">
				<div class="col-md-4">
					<div class="box">
						<a href="<?php echo get_permalink($recent->ID); ?>">
							<img src="<?php echo get_template_directory_uri(); ?>-child/images/uc1.png" class="header1">

							<p>Pure-Software solutionthat achieves Hardware-grade security</p>
						</a>
					</div>
				</div>
				<div class="col-md-4">
					<div class="box">
						<a href="<?php echo get_permalink($recent->ID); ?>">
							<img src="<?php echo get_template_directory_uri(); ?>-child/images/uc2.png" class="header1">
							<p>No dependency on underlying hardware and physical infrastructure (e.g. HSM)</p>
						</a>
					</div>
				</div>
				<div class="col-md-4">
					<div class="box">
						<a href="<?php echo get_permalink($recent->ID); ?>">
							<img src="<?php echo get_template_directory_uri(); ?>-child/images/uc3.png" class="header1">
							<p>Central key management and audit across distributed environments</p>
						</a>
					</div>
				</div>
			</div>
	    </div>
	    </div>
	</div>
		
	
	<div class="tabs-usecast">	
		<h2>Here is how some of our customers are deploying Unbound in their organization.</h2>
		<div class="row tabs">
            <div class="col-md-6 tab selected" data-tab="hybrid">
	            <div class="lim"><b>1.</b>Hybrid environments with multiple workloads</div>
	        </div>
            <div class="col-md-6 tab only-desktop" data-tab="global">
	            <div class="lim"><b>2.</b>Global organization with multiple branches spread worldwide </div>
            </div>
        </div>
        
        <div class="tabs-content tab1" id="tab-hybrid">
	        <p>
		        <img src="<?php echo get_template_directory_uri(); ?>-child/images/d1.png" class="pull-right t50">

		        A large enterprise was operating security across multiple underlying infrastructures, caused by aggressive M&A activities and infrastructure that was still not fully integrated. The enterprise looked to consolidate key management operations across cloud and on premises, while leveraging its existing investment in an on-prem HSM. Unbound Crypto-Orchestration unified the disparate key management systems into a central managed platform that delivered the highest level of security. </p>
        </div>
        
        <div class="row tabs only-mobile">
            <div class="col-md-6 tab" data-tab="global">
	            <div class="lim"><b>2.</b>Global organization with multiple branches spread worldwide </div>
            </div>
        </div>
        
        <div class="tabs-content tab1" id="tab-global">
	        <p>
		        <img src="<?php echo get_template_directory_uri(); ?>-child/images/d2.png" class="pull-right t50">

		        A large manufacturing company with headquarters in the U.S. and production plants distributed across several countries in Asia struggled with controlling the security of keys in off-shore facilities. Unbound Crypto Orchestration creates an abstracted key management layer to protect and manage crypto-keys across all facilities and all geographies. </p>
        </div>
	</div>
	
	<div class="streamlined">
	    <div class="container">
	        <div class="intor">
		        <h2>Key Management is now<br>Streamlined to Your Needs</h2>
		        <p><b>Unbound Crypto Orchestration</b> centralizes key management across the organization, while maintaining hardware-level security.</p>
				<a href="javascript:;" class="cta">( read more about Crypto Orchestration )</a>
	        </div>
	        <div class="row">
				<div class="col-md-4">
					<div class="box">
						<p><b>Seamless Integration</b><br>

Integrates seamlessly with common HSMs and key management systems.
</p>
					</div>
				</div>
				<div class="col-md-4">
					<div class="box">
						<p><b>Deploy Everywhere</b><br>Easily deployed everywhere, just like any other software: cloud, on premises, and anywhere acrossthe globe. </p>
					</div>
				</div>
				<div class="col-md-4">
					<div class="box">
						<p><b>Any System</b><br>
Crypto-Orchestration runs on standard HW, physical or virtual machines, or container environments. 
</p>
					</div>
				</div>
			</div>
	        <div class="row">
				<div class="col-md-4">
					<div class="box">
						<p><b>Centralized Key Control</b><br>
Unified key management and protection for distributed infrastructure - either spread geographically or across a hybrid environment. </p>
					</div>
				</div>
				<div class="col-md-4">
					<div class="box">
						<p><b>An End To Vendor Lock-In</b><br>
Unbound is completely agnostic and compatible with all key management systems. </p>
					</div>
				</div>
				<div class="col-md-4">
					<div class="box">
						<p><b>Mathematically Proven Security</b><br>
Unbound pure-SW solution draws its trust from mathematically proven algorithms which allows it to achieves HW-grade security for cryptographic keys and secrets.</p>
					</div>
				</div>
			</div>
	    </div>
	</div>
	
	<div class="learn-more">
		<h2>Want to Learn More?</h2>
		<p>
			<a href="javascript:;" class="cta">( Let’s Talk )</a>
			<a href="javascript:;" class="cta">( Download Solution Brief )</a>
		</p>
	</div>
	
</article><!-- #post-<?php the_ID(); ?> -->
